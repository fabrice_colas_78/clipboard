// ClipEntry.cpp: implementation of the CClipEntry class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MultiClip.h"
#include "ClipEntry.h"

#include <log.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

int CClipEntry::m_sHandleAllocoted = 0;

HANDLE CClipEntry::HandleAlloc(DWORD dwSize)
{
	HANDLE h = GlobalAlloc(GMEM_FIXED, dwSize);
	if (h == NULL)
	{
		ASSERT(false);
		return NULL;
	}
	m_sHandleAllocoted++;
	LOG1F("AllocHandle count:%d.\n", m_sHandleAllocoted);
	return h;
}

void CClipEntry::HandleFree(HANDLE h)
{
	GlobalFree(h);
	m_sHandleAllocoted--;
	LOG1F("FreeHandle count:%d.\n", m_sHandleAllocoted);
}

DWORD CClipEntry::HandleSize(HANDLE h)
{
	return GlobalSize(h);
}

LPVOID CClipEntry::HandleLock(HANDLE h)
{
	return GlobalLock(h);
}

BOOL CClipEntry::HandleUnlock(HANDLE h)
{
	return GlobalUnlock(h);
}

int CClipEntry::GetHandleAllocated()
{
	return m_sHandleAllocoted;
}

CClipEntry::CClipEntry()
{
	m_hData = NULL;
	m_uFormat = 0;
}

CClipEntry::CClipEntry(HANDLE hData, UINT uFormat)
{
	m_hData = hData;
	m_uFormat = uFormat;
}

CClipEntry::~CClipEntry()
{
}

void CClipEntry::SetHandleAndFormat(HANDLE hData, UINT uFormat)
{
	HandleFree();
	m_hData = hData;
	m_uFormat = uFormat;
}

void CClipEntry::HandleFree()
{
	if (m_hData) HandleFree(m_hData);
}


/*void CClipEntry::operator=(const CClipEntry& clipentry)
{
	m_hData   = clipentry.m_hData;
	m_uFormat = clipentry.m_uFormat;
}*/

BOOL CClipEntry::CopyToClipboard(HWND hWnd, HANDLE hData)
{
	if (!::OpenClipboard(hWnd)) return false;
	if (EmptyClipboard())
	{
		HANDLE hNewData = CopyHandle(hData);
		SetClipboardData(CF_TEXT, hNewData);
	}
	CloseClipboard();
	return true;
}

HANDLE CClipEntry::MakeHandle(LPVOID datas, DWORD dwSize)
{
	HANDLE hData = HandleAlloc(dwSize);
	if (hData == NULL) return NULL;
	LPVOID lpDatas = HandleLock(hData);
	if (lpDatas == NULL)
	{
		HandleFree(hData);
		return NULL;
	}
	memcpy(lpDatas, datas, dwSize);
	HandleUnlock(hData);
	return hData;
}

HANDLE CClipEntry::CopyHandle(HANDLE hData)
{
	if (hData == NULL) return NULL;

	LPVOID lpData = HandleLock(hData);
	if (lpData == NULL) return NULL;

	DWORD dwSize = GlobalSize(hData);
	HANDLE hNewData = HandleAlloc(dwSize);
	if (hNewData)
	{
		LPVOID lpNewData = HandleLock(hNewData);
		if (lpNewData)
		{
			memcpy(lpNewData, lpData, dwSize);
		}
		HandleUnlock(hNewData);
	}
	HandleUnlock(hData);
	return hNewData;
}

BOOL CClipEntry::CompareHandle(HANDLE h1, HANDLE h2)
{
	LOG2F("CClipEntry::CompareHandle\n");
	//	Compare size
	if (GlobalSize(h1) != GlobalSize(h2)) return false;

	LOG2F("CompareHandle size=same\n");

	//	Compare data
	LPVOID lp1 = HandleLock(h1);
	if (lp1 == NULL)
	{
		return false;
	}

	LPVOID lp2 = HandleLock(h2);
	if (lp2 == NULL)
	{
		HandleUnlock(h1);
		return false;
	}

	int ret = memcmp(lp1, lp2, GlobalSize(h1));

	HandleUnlock(h1);
	HandleUnlock(h2);

	return ret == 0;
}

void CClipEntry::ConvertDatasToBCD(LPTSTR szDatas, int nLenszDatas, void * HexDatas, int nLenHexDatas)
{
	int nLen = min(nLenszDatas / 3 - 1, nLenHexDatas);
	for (int i = 0; i < nLen; i++)
	{
		sprintf(szDatas + strlen(szDatas), "%02X", ((int)((char *)HexDatas)[i]) & 0xff);
		if (i < (nLen - 1)) strcat(szDatas, ",");
	}
}

void CClipEntry::ConvertDatasToText(LPTSTR  szBufferDatas, int nLenszDatas, LPCTSTR TextDatas, int nLenText, BOOL fConvertAnd)
{
#ifdef _DEBUG
	static int n = 0;
	n++;
#endif
	const char * p = TextDatas;
	char * tag;
	int i = 0;
	for (int nIndexText = 0;; nIndexText++)
	{
#ifdef _DEBUG
		//LOG1F("n=%d i=%d nLenText=%d TextDatas-p=%d nLenszDatas-7=%d\n", n, i, nLenText, TextDatas-p, nLenszDatas-7);
#endif
		if (i >= nLenszDatas - 7)
		{
			//	No more buffer
			//LOG1F("ConvertDatasToText No more buffer i=%d\n", i);
			tag = "...";
			strcpy(szBufferDatas + i, tag);
			i += strlen(tag);
			break;
		}
		if (nIndexText >= nLenText)
		{
			//	No more text
			//LOG1F("ConvertDatasToText No more text\n");
			break;
		}
		if ((TextDatas[nIndexText] < 32) || (TextDatas[nIndexText] > 126))
		{
			switch (TextDatas[nIndexText])
			{
			case '\t':
				tag = "<tab>";
				strcpy(szBufferDatas + i, tag);
				i += strlen(tag);
				break;
			case '\r':
				tag = "<cr>";
				strcpy(szBufferDatas + i, tag);
				i += strlen(tag);
				break;
			case '\n':
				tag = "<lf>";
				strcpy(szBufferDatas + i, tag);
				i += strlen(tag);
				break;
			default:
				szBufferDatas[i++] = TextDatas[nIndexText];
				break;
			}
		}
		else if (TextDatas[nIndexText] == '&')
		{
			if (fConvertAnd)
			{
				tag = "&&";
				strcpy(szBufferDatas + i, tag);
				i += strlen(tag);
			}
			else
			{
				szBufferDatas[i++] = TextDatas[nIndexText];
			}
		}
		else
		{
			szBufferDatas[i++] = TextDatas[nIndexText];
		}
	}
	szBufferDatas[i] = '\0';
}
