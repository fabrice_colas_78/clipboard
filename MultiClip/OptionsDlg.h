#if !defined(AFX_OPTIONSDLG_H__B0559446_C57D_4375_B97E_D7BA44C97F6A__INCLUDED_)
#define AFX_OPTIONSDLG_H__B0559446_C57D_4375_B97E_D7BA44C97F6A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// OptionsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// COptionsDlg dialog

#include "HotkeyCtrlEx.h"

class COptionsDlg : public CDialog
{
	// Construction
public:
	DWORD m_dwHotkeyPopupMenuShiftInsert, m_dwHotkeyPopupMenuCtrlV;
	DWORD m_dwHotkeyInsertSpecialClipboard;
	DWORD m_dwHotkeyCopySpecialBuf1, m_dwHotkeyPasteSpecialBuf1, m_dwHotkeyCopySpecialBuf2, m_dwHotkeyPasteSpecialBuf2;
	COptionsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(COptionsDlg)
	enum { IDD = IDD_DIALOG_OPTIONS };
	CHotKeyCtrlEx	m_hkPopupMenuCtrlV;
	CHotKeyCtrlEx	m_hkPopupMenuShiftInsert;
	CHotKeyCtrlEx	m_hkSpecialClipboard;
	CHotKeyCtrlEx	m_hkCopySpecialBuf1;
	CHotKeyCtrlEx	m_hkPasteSpecialBuf1;
	CHotKeyCtrlEx	m_hkCopySpecialBuf2;
	CHotKeyCtrlEx	m_hkPasteSpecialBuf2;
	BOOL	m_fStartWithSession;
	UINT	m_nMaxClipEntries;
	BOOL	m_fNoMultipleEntry;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COptionsDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(COptionsDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPTIONSDLG_H__B0559446_C57D_4375_B97E_D7BA44C97F6A__INCLUDED_)
