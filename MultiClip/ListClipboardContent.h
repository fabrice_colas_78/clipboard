#if !defined(AFX_LISTCLIPBOARDCONTENT_H__4E8B2AD5_1589_4068_89A5_2FA6EFEB1A16__INCLUDED_)
#define AFX_LISTCLIPBOARDCONTENT_H__4E8B2AD5_1589_4068_89A5_2FA6EFEB1A16__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ListClipboardContent.h : header file
//

#include <listctrlex.h>
#include "ClipEntry.h"


/////////////////////////////////////////////////////////////////////////////
// CListClipboardContent window

class CListClipboardContent : public CListCtrlEx2
{
	// Construction
public:
	enum { CI_SIZE, CI_COUNT, CI_DATA, CI_FORMAT, CI_FORMATID };

	BOOL AddClipboardFormat(int nIndex, UINT uFormat, HANDLE hDatas, BOOL fNoMultipleEntry, BOOL fDoNotFreeHandle = false);
	BOOL SetClipboardFormat(int nIndex, UINT uFormat, HANDLE hDatas);
	CListClipboardContent();

	// Attributes
public:

	// Operations
public:

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CListClipboardContent)
		//}}AFX_VIRTUAL

	// Implementation
public:
	void IncCount(int nIndex);
	void SetCount(int nIndex, int nCount);
	int  GetCount(int nIndex);

	UINT   GetFormatID(int nIndex);
	UINT   GetCurrentFormatID();
	HANDLE GetDataHandle(int nIndex);
	HANDLE GetCurrentDataHandle();

	void DeleteAllItems();
	void DeleteItem(int nIndex);
	void Init();
	virtual ~CListClipboardContent();

	// Generated message map functions
protected:
	//{{AFX_MSG(CListClipboardContent)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
private:
	CClipEntry * GetClipEntry(int nIndex);
	BOOL SetClipEntry(int nIndex, CClipEntry * pClipEntry);
	BOOL IsClipboardEntryExist(UINT uFormat, HANDLE hDatas);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LISTCLIPBOARDCONTENT_H__4E8B2AD5_1589_4068_89A5_2FA6EFEB1A16__INCLUDED_)
