// ClipEntry.h: interface for the CClipEntry class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLIPENTRY_H__BFE2F9A3_BD80_4348_A93B_B6EC48079F4A__INCLUDED_)
#define AFX_CLIPENTRY_H__BFE2F9A3_BD80_4348_A93B_B6EC48079F4A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxtempl.h>

class CClipEntry
{
public:
	CClipEntry();
	CClipEntry(HANDLE hData, UINT uFormat);
	~CClipEntry();
	void SetHandleAndFormat(HANDLE hData, UINT uFormat);
	void HandleFree();
	void operator=(const CClipEntry& clipentry);
	HANDLE GetHandle() { return m_hData; }
	UINT GetFormat() { return m_uFormat; }

	static BOOL CompareHandle(HANDLE h1, HANDLE h2);
	static HANDLE CopyHandle(HANDLE hData);
	static HANDLE MakeHandle(LPVOID datas, DWORD dwSize);
	static void ConvertDatasToBCD(LPTSTR szDatas, int nLenszDatas, void * HexDatas, int nLenHexDatas);
	static void ConvertDatasToText(LPTSTR szDatas, int nLenszDatas, LPCTSTR TextDatas, int nLenText, BOOL fConvertAnd = false);
	static BOOL CopyToClipboard(HWND hWnd, HANDLE hData);

	static HANDLE HandleAlloc(DWORD dwSize);
	static void HandleFree(HANDLE h);
	static DWORD HandleSize(HANDLE h);
	static LPVOID HandleLock(HANDLE h);
	static BOOL HandleUnlock(HANDLE h);

	static int GetHandleAllocated();

private:
	UINT   m_uFormat;
	HANDLE m_hData;
	static int m_sHandleAllocoted;
};

//typedef CArray <class CClipEntry, class CClipEntry&> CClipEntries;


#endif // !defined(AFX_CLIPENTRY_H__BFE2F9A3_BD80_4348_A93B_B6EC48079F4A__INCLUDED_)
