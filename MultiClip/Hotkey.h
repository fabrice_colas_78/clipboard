// Hotkey.h: interface for the CHotkey class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HOTKEY_H__63E957F3_BF9C_4AB5_9677_536406CB54B2__INCLUDED_)
#define AFX_HOTKEY_H__63E957F3_BF9C_4AB5_9677_536406CB54B2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <log.h>
#include <winerr.h>

#define NO_VIRTUAL_KEY	0xFFFF
#define NO_MODIFIER		0xFFFF
#define NO_ID			0xFFFFFFFF

class CHotkey
{
public:
	static UINT GetFirstFreeHotkeyId();
	static BOOL IsRegisterableHotkey(HWND hWnd, DWORD dwVirtualKeyAndModifiers);
	static BOOL RegisterHotkey(HWND hWnd, DWORD dwVirtualKeyAndModifiers, UINT& uIdHotkey);
	static BOOL IsRegisterableHotkey(HWND hWnd, WORD wVirtualKey, WORD wModifiers);
	static WORD GetHotkeyModifiers(DWORD dwVirtualKeyAndModifiers);
	static WORD GetHotkeyVirtualKeyCode(DWORD dwVirtualKeyAndModifiers);
	static DWORD MakeHotkey(WORD wVirtualKey, WORD wModifiers);
	static WORD ConvertModifiersToCtrl(WORD wModifiers);
	static WORD ConvertModifiersFromCtrl(WORD wModifiersCtrl);
	static DWORD ConvertHotkeyFromCtrl(DWORD dwVirtualKeyAndModifiers);

	static CString GetHostkeyName(DWORD dwVirtualKeyAndModifiers);
	static CString GetHostkeyName(WORD wVirtualKey, WORD wModifiers);
	static BOOL RegisterHotkey(HWND hWnd, WORD wVirtualKey, WORD wModifiers, UINT& uIdHotkey);
	static BOOL UnRegisterHotkey(HWND hWnd, UINT uIdHotkey);

	CHotkey::CHotkey();
	BOOL RegisterHotkey(HWND hwndOwner, WORD wVirtualKey, WORD wModifiers);
	BOOL RegisterHotkey(HWND hwndOwner, DWORD dwVirtualKeyAndModifiers);
	BOOL UnRegisterHotkey();

	void operator=(const CHotkey& hotkey);

	UINT GetHotkeyID() const;
	CString GetHotkeyName() const;
	BOOL IsRegistered() const;
	BOOL IsValid() const;

private:
	HWND m_hwndOwner;
	WORD m_wVirtualKey;
	WORD m_wModifiers;
	UINT m_uHotkeyID;
};

#endif // !defined(AFX_HOTKEY_H__63E957F3_BF9C_4AB5_9677_536406CB54B2__INCLUDED_)
