// HotKeyCtrlEx.cpp : implementation file
//

#include "stdafx.h"
#include "MultiClip.h"
#include "HotKeyCtrlEx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHotKeyCtrlEx

CHotKeyCtrlEx::CHotKeyCtrlEx()
{
}

CHotKeyCtrlEx::~CHotKeyCtrlEx()
{
}


BEGIN_MESSAGE_MAP(CHotKeyCtrlEx, CHotKeyCtrl)
	//{{AFX_MSG_MAP(CHotKeyCtrlEx)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHotKeyCtrlEx message handlers

void CHotKeyCtrlEx::SetHotKeyToCtrl(DWORD dwVirtualKeyCodeAndModifiers)
{
	CHotKeyCtrlEx::SetHotKey(CHotkey::GetHotkeyVirtualKeyCode(dwVirtualKeyCodeAndModifiers),
		CHotkey::ConvertModifiersToCtrl(CHotkey::GetHotkeyModifiers(dwVirtualKeyCodeAndModifiers)));
}

DWORD CHotKeyCtrlEx::GetHotKeyFromCtrl()
{
	WORD wVirtualKey, wModifiers;
	GetHotKey(wVirtualKey, wModifiers);
	return CHotkey::MakeHotkey(wVirtualKey, CHotkey::ConvertModifiersFromCtrl(wModifiers));
}
