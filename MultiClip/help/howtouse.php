<?
include "helpdefs.phi";

WriteHeader(APPLICATION_NAME  . " - Index");

PageTitle("Fonctionnement de " . APPLICATION_NAME);
Hr();

Out("Au d�marrage " . APPNAME . " se positionne dans la barre de notification windows avec l'ic�ne suivante:"
 . Img("trayicon.gif"));
Br();
OutB("Principe:");
Br();
Out("A chaque \"copier\" dans le presse-papier, et ce depuis n'importe quelle application, une entr�e est cr��e dans " . APPNAME 
 . " dans l'historique des entr�es.");
Out("Pour rappeler une entr�e de " . APPNAME . " et la \"coller\" dans une application, il suffit d'activer le " .RefPopup(). " avec la s�quence "
 . "de touche d�finie dans les " . RefPref() . ". Le coller peut �tre simul� soit avec la s�quence de touche standard [Ctrl-V] ou bien pour certaines "
 . "applications [Shift-Insert].");
Br();
Out("<b>A noter</b>: seules les entr�es de type texte sont intercept�es par ". APPNAME . ".");
Br();
Out("Pour le r�glage des options, faites un simple clic gauche sur cette ic�ne, appuyer ensuite sur le bouton " . RefPref() . " et "
. "veuillez vous reporter � la section " . RefPref());
Br();
Br();
OutB("Fen�tre principale de " . APPNAME);
Br();Br();
Center(Img("mainwindow.gif"));
Br();
Br();
Out("Au dessus de chaque liste, le nombre d'entr�e d�j� enregistr�es sur le nombre d'entr�es maximum est indiqu�.");
Br();
Br();
DisplayItem("Historique des entr�es", "Cette liste contient les derni�res entr�es enregistr�es par " . APPNAME
. Li("La premi�re colonne indique la longeur de la chaine de caract�res m�moris�e")
. Li("La deuxi�me colonne indique le nombre de fois ou l'occurence  a �t� ins�r�e dans le presse-papier")
. Li("La troisi�me colonne indique "));

DisplayItem("Enr�es sp�ciales", "Cette liste contient des entr�es explicitement misent dedans."
. " Ces entr�es sont accessibles dans le " .RefPopup(). " [Entr�es sp�ciales]");

Out("<b>NB:</b> Un clic droit sur chacune des 2 listes permet d'afficher un menu contextuel de " . HRef(POPUPLIST_PAGE, "gestion des listes"));
Br();
Br();

DisplayItem("Toujours visible", "Si cette option est activ�e, la fen�tre principale restera constemment au dessus des autres fen�tres (TopMost)");

DisplayItem("Pr�f�rences", "Ce bouton vous permet d'acc�der aux " . RefPref() . " de " . APPNAME);

WriteTailer();

?>