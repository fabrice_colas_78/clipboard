<?
include "helpdefs.phi";

WriteHeader(APPLICATION_NAME  . " - Index");

PageTitle("Menu popup d'insertion des entr�es");
Hr();
Br();
Center(Img("popup.gif"));
Br();
DisplayItem("1..Z", "La partie haute du menu contient l'historique des entr�es de la plus r�cente � la plus ancienne.");

DisplayItem("+ Entr�es sp�ciales", "Pour d�plier ce menu, il vous suffit d'appuyer sur la touche d'acc�s rapide '+'."
 . " Ce menu contient les entr�es ins�r�es explicitement. Ces entr�es sont sauvegard�es m�me � l'arr�t/red�marrage d'une autre session.");
DisplayItem("- Ajouter une entr�e sp�ciale", "Ce menu vous permet d'acc�der directement � la boite de dialogue d'ajout d'une nouvelle entr�e");
DisplayItem("/ Pr�f�rences", "Ce menu vous permet d'acc�der directement � la boite de dialogue des " . RefPref());
DisplayItem("* Gestionnaire", "Ce menu vous permet d'acc�der directement � la fen�tre de gestion principale");
DisplayItem("0 Annuler", "N'ex�cute aucune commande");

WriteTailer();

?>