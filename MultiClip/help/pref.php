<?
include "helpdefs.phi";

WriteHeader(APPLICATION_NAME  . " - Index");

PageTitle("Pr�f�rences de " . APPLICATION_NAME);
Hr();

Center(Img("pref.gif"));
Br();

DisplayItem("Nombre d'entr�es historis�e maximum", 
"Ce param�tre d�finit le nombre maximum d'entr�es qui seront conserv�es dans le buffer de " . APPNAME);

DisplayItem("Supprimer les entr�es multiples", 
	"Assure l'unicit� des entr�es dans le buffer de " . APPNAME);

DisplayItem("Ex�cuter au d�marrage de la session Windows", 
	"Ex�cute " . APPNAME . " au d�marrage de la session utilisateur");

DisplayItem("S�quence de touches activant le menu d�roulant. Simulation (Ctrl-V)",
	  "Affiche un menu d�roulant avec l'historique des entr�es (de la plus r�cente � la plus ancienne). "
	. "Une fois l'entr�e s�lectionn�e, le texte est copi� dans le presse-papier puis la s�quence de touche [Ctrl-V] est envoy�e � l'application courante");
	
DisplayItem("S�quence de touches activant le menu d�roulant. Simulation (Shift-Insert)",
	"Idem option pr�c�dente mais envoi de la s�quence de touches [Shift-Insert]. En effet, certaines applications n'accepte pas la s�quence de touches [Ctrl-V] (ex: �mulateur de terminaux)");
	
WriteTailer();

?>