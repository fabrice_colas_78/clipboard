<?

include "helpdefs.phi";

WriteHeader(APPLICATION_NAME  . " - Index");

PageTitle("Bienvenue dans " . APPNAME);
Hr();

OutB("Bienvenue dans " . APPNAME);
Br();Br();
Out("Le fonctionnement standard du presse-papier Windows ne permet de ne conserver que la derni�re entr�e copi�e dans celui-ci.");
Out(APPLICATION_NAME . " est un programme de mise en m�moire du presse-papier Windows.");
Br();
Out("Il m�morise les derni�res entr�es qui ont �t� copi�es dans le presse-papier m�me lorsque celles-ci auront �t� �cras�es par une autre "
 . "application ou une nouvelle entr�e. Le nombre de ces entr�es est d�finissable � l'aide d'un param�tre dans les " 
 . RefPref() . " de l'application.");
Br();
Out("Une s�quence de touche, elle aussi param�trable dans les " . RefPref() . ","
	. " permet d'afficher toutes les entr�es au travers d'un " .RefPopup(). " et, une fois la s�lection "
	. "effectu�e, de \"coller\" le contenu de l'entr�e dans l'application courante. "
	. " Cette s�quence de touche �quivaut � un [Ctrl-V] classique dans l'application courante.");
Br();
Out("Pour les applications ne prenant pas en compte la s�quence de touche [Ctrl-V], une deuxi�me "
	. "s�quence de touche est pr�vue pour �muler le [Shift-Insert].");
Br();
Br();
Br();

Br();
WriteTailer();
?>