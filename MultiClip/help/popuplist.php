<?
include "helpdefs.phi";

WriteHeader(APPLICATION_NAME  . " - Index");

PageTitle("Menu de gestion des listes d'entr�es");
Hr();
Br();
Center(Img("popuplist.gif"));
Br();
DisplayItem("Copier cette entr�e dans le presse-papier", "Copie de fa�on explicite l'entr�e s�lectionn�e dans le presse-papier");
DisplayItem("Editier cette entr�e", "Permet de consulter/�diter l'entr�e s�lectionn�e.");
DisplayItem("Supprimer cette entr�e", "Supprime l'entr�e s�lectionn�e de la liste");
DisplayItem("Copier cette entr�e dans l'autre liste", "Fait un copie de l'entr�e s�lectionn�e dans l'autre liste");
DisplayItem("Ajouter une entr�e", "Permet l'edition/ajout d'une nouvelle entr�e dans la liste");

WriteTailer();

?>