@echo off

echo CommandLine: %0 %*

set DROPBOX_FOLDER="C:\Users\Fabrice\Dropbox\Public\MultiClip"

set COMMON_INSTALLER_CMD=%~d0\Batch\MakeInstaller.cmd

set PROJECT_DIR=%1
set OUTPUT_DIR=%2
set OUTPUT_FILE=%3
set SPECIFIC_RELEASE=%4

set BUILD_DATE=%date:~6,4%-%date:~3,2%-%date:~0,2%

set BUILD=
for /F "tokens=3" %%i in (%~dp0\..\res\Build.txt) do set BUILD=%%i

set VERSION=
for /F "tokens=3" %%i in (%~dp0\..\res\Version.txt) do call :buildVersion %%i

set VERSIONED_MANTICE=%VERSION%-b%BUILD%-%BUILD_DATE%

echo PROJECT_DIR:%PROJECT_DIR%
echo OUTPUT_DIR :%OUTPUT_DIR%
echo OUTPUT_FILE:%OUTPUT_FILE%
echo SPECIFIC_RELEASE:%SPECIFIC_RELEASE%
echo BUILD_DATE:%BUILD_DATE%
echo BUILD:%BUILD%
echo VERSION:%VERSION%
echo VERSIONED_MANTICE:%VERSIONED_MANTICE%
rem echo REMOTE_USER:%REMOTE_USER%
rem echo REMOTE_HOST:%REMOTE_HOST%
rem echo REMOTE_DIR:%REMOTE_DIR%

rem default INSTALLER_PATH is %TEMP%
set INSTALLER_PATH=U:\Delivery

set FILES_LIST_TO_SEND=

rem Help
call %~dp0MakeHelp.cmd

set INSTALLER_VERSIONED=
set INSTALLER_VERSIONED=%VERSIONED_MANTICE%

set INSTALLER_FILE=MultiClip%SPECIFIC_RELEASE%Installer
set APPLICATION_NAME=Multi Clipboard%SPECIFIC_RELEASE%
set WINDOWS_LIST=Multi Clipboard%SPECIFIC_RELEASE%
set FILES_LIST=%OUTPUT_FILE%
set SHORTCUT_NAME=Multi Clipboard%SPECIFIC_RELEASE%
call %COMMON_INSTALLER_CMD% %INSTALLER_FILE% "%APPLICATION_NAME%" "%WINDOWS_LIST%" "%FILES_LIST%" "%SHORTCUT_NAME%"
if "_%INSTALLER_FILE_VERSIONED%_" neq "__" set FILES_LIST_TO_SEND=%FILES_LIST_TO_SEND% %INSTALLER_FILE_VERSIONED%
set ZIP_FILE="%INSTALLER_PATH%\MultiClip_%VERSIONED_MANTICE%.zip"
echo Zipping %ZIP_FILE%...
zip -j -u %ZIP_FILE% %OUTPUT_FILE%
echo Copying zip file %ZIP_FILE% to %DROPBOX_FOLDER%...
copy /Y %ZIP_FILE% %DROPBOX_FOLDER%
set FILES_LIST_TO_SEND=%FILES_LIST_TO_SEND% %ZIP_FILE%

goto final

set HELP_FILE=MultiClip
set HELP_TITLE=Aide de Multi Clipboard
call w:\Common\MakeHelp\MakeHelp.cmd %HELP_FILE% "%HELP_TITLE%"


Set INSTALLER_FILE=MultiClipInstaller
set APPLICATION_NAME=Multi Clipboard
set WINDOWS_LIST=Multi Clipboard
set FILES_LIST=release\MultiClip.exe;help\MultiClip.chm
set SHORTCUT_NAME=Multi Clipboard
call w:\Common\MakeInstaller.cmd %INSTALLER_FILE% "%APPLICATION_NAME%" "%WINDOWS_LIST%" "%FILES_LIST%" "%SHORTCUT_NAME%"

goto final

:buildVersion
if _%VERSION%_ neq __ set VERSION=%VERSION%.%1
if _%VERSION%_ equ __ set VERSION=%1
goto :eof


:final