//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MultiClip.rc
//
#define IDS_MAIN_TITLE                  1
#define IDD_MULTICLIP_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDR_HISTO                       129
#define IDD_DIALOG_OPTIONS              130
#define IDD_DIALOG_CLIP_CONTENT         131
#define IDC_BUTTON_QUIT                 1000
#define IDC_BUTTON_ABOUT                1001
#define IDC_LIST_CLIP_CONTENT           1002
#define IDC_BUTTON_OPTIONS              1003
#define IDC_BUTTON_HELP                 1004
#define IDC_CHECK_START_WITH_SESSION    1005
#define IDC_LIST_SPECIAL_CLIP_CONTENT   1005
#define IDC_EDIT_MAX_ENTRIES            1006
#define IDC_CHECK_NO_MULTIPLE_ENTRY     1007
#define IDC_STATIC_ENTRIES_COUNT        1008
#define IDC_STATIC_SPECIAL_ENTRIES_COUNT 1009
#define IDC_EDIT_CLIP_CONTENT           1010
#define IDC_HOTKEY_POPUPMENU            1011
#define IDC_HOTKEY_POPUPMENU_CTRLV      1011
#define IDC_CHECK_ALWAYS_ON_TOP         1012
#define IDC_HOTKEY_SPECIAL_CLIPBOARD    1012
#define IDC_HOTKEY_POPUPMENU_SHIFTINSERT 1013
#define IDC_BUTTON_REGISTER_UNREGISTER  1013
#define IDC_SLIDER_TRANSPARENCY         1014
#define IDC_HOTKEY_SPECIAL_INSERT_BUF1  1015
#define IDC_STATIC_SPECIAL_BUF1         1015
#define IDC_HOTKEY_SPECIAL_PASTE_BUF1   1016
#define IDC_STATIC_SPECIAL_BUF2         1016
#define IDC_HOTKEY_SPECIAL_INSERT_BUF2  1019
#define IDC_HOTKEY_SPECIAL_PASTE_BUF2   1020

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
