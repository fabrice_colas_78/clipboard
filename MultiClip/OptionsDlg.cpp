// OptionsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MultiClip.h"
#include "OptionsDlg.h"

#include "hotkey.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// COptionsDlg dialog


COptionsDlg::COptionsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(COptionsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(COptionsDlg)
	m_fStartWithSession = FALSE;
	m_nMaxClipEntries = 0;
	m_fNoMultipleEntry = FALSE;
	//}}AFX_DATA_INIT
}


void COptionsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COptionsDlg)
	DDX_Control(pDX, IDC_HOTKEY_POPUPMENU_CTRLV, m_hkPopupMenuCtrlV);
	DDX_Control(pDX, IDC_HOTKEY_POPUPMENU_SHIFTINSERT, m_hkPopupMenuShiftInsert);
	DDX_Control(pDX, IDC_HOTKEY_SPECIAL_CLIPBOARD, m_hkSpecialClipboard);
	DDX_Control(pDX, IDC_HOTKEY_SPECIAL_INSERT_BUF1, m_hkCopySpecialBuf1);
	DDX_Control(pDX, IDC_HOTKEY_SPECIAL_PASTE_BUF1, m_hkPasteSpecialBuf1);
	DDX_Control(pDX, IDC_HOTKEY_SPECIAL_INSERT_BUF2, m_hkCopySpecialBuf2);
	DDX_Control(pDX, IDC_HOTKEY_SPECIAL_PASTE_BUF2, m_hkPasteSpecialBuf2);
	DDX_Check(pDX, IDC_CHECK_START_WITH_SESSION, m_fStartWithSession);
	DDX_Text(pDX, IDC_EDIT_MAX_ENTRIES, m_nMaxClipEntries);
	DDV_MinMaxUInt(pDX, m_nMaxClipEntries, 5, 100);
	DDX_Check(pDX, IDC_CHECK_NO_MULTIPLE_ENTRY, m_fNoMultipleEntry);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COptionsDlg, CDialog)
	//{{AFX_MSG_MAP(COptionsDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COptionsDlg message handlers

BOOL COptionsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO: Add extra initialization here
	//LOG2F("HotkeyName
	m_hkSpecialClipboard.SetHotKeyToCtrl(m_dwHotkeyInsertSpecialClipboard);
	m_hkPopupMenuCtrlV.SetHotKeyToCtrl(m_dwHotkeyPopupMenuCtrlV);
	m_hkPopupMenuShiftInsert.SetHotKeyToCtrl(m_dwHotkeyPopupMenuShiftInsert);
	m_hkCopySpecialBuf1.SetHotKeyToCtrl(m_dwHotkeyCopySpecialBuf1);
	m_hkPasteSpecialBuf1.SetHotKeyToCtrl(m_dwHotkeyPasteSpecialBuf1);
	m_hkCopySpecialBuf2.SetHotKeyToCtrl(m_dwHotkeyCopySpecialBuf2);
	m_hkPasteSpecialBuf2.SetHotKeyToCtrl(m_dwHotkeyPasteSpecialBuf2);
	UpdateData(false);
	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

void COptionsDlg::OnOK()
{
	// TODO: Add extra validation here
	UpdateData(true);
	m_dwHotkeyInsertSpecialClipboard = m_hkSpecialClipboard.GetHotKeyFromCtrl();
	m_dwHotkeyPopupMenuCtrlV = m_hkPopupMenuCtrlV.GetHotKeyFromCtrl();
	m_dwHotkeyPopupMenuShiftInsert = m_hkPopupMenuShiftInsert.GetHotKeyFromCtrl();
	m_dwHotkeyCopySpecialBuf1 = m_hkCopySpecialBuf1.GetHotKeyFromCtrl();
	m_dwHotkeyPasteSpecialBuf1 = m_hkPasteSpecialBuf1.GetHotKeyFromCtrl();
	m_dwHotkeyCopySpecialBuf2 = m_hkCopySpecialBuf2.GetHotKeyFromCtrl();
	m_dwHotkeyPasteSpecialBuf2 = m_hkPasteSpecialBuf2.GetHotKeyFromCtrl();
	CDialog::OnOK();
}
