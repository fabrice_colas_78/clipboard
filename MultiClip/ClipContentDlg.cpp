// ClipContentDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MultiClip.h"
#include "ClipContentDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CClipContentDlg dialog


CClipContentDlg::CClipContentDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CClipContentDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CClipContentDlg)
	m_sClipContent = _T("");
	//}}AFX_DATA_INIT
}


void CClipContentDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CClipContentDlg)
	DDX_Control(pDX, IDC_EDIT_CLIP_CONTENT, m_ebClipContent);
	DDX_Text(pDX, IDC_EDIT_CLIP_CONTENT, m_sClipContent);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CClipContentDlg, CDialog)
	//{{AFX_MSG_MAP(CClipContentDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CClipContentDlg message handlers

BOOL CClipContentDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO: Add extra initialization here
	UpdateData(false);
	m_ebClipContent.SetSel(-1, -1);
	return TRUE;  // return TRUE unless you set the focus to a control
				  // EXCEPTION: OCX Property Pages should return FALSE
}

void CClipContentDlg::OnOK()
{
	// TODO: Add extra validation here
	UpdateData(true);
	CDialog::OnOK();
}
