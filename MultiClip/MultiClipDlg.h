// MultiClipDlg.h : header file
//

#if !defined(AFX_MULTICLIPDLG_H__397C8831_AC75_43CC_AD72_68D514B9395B__INCLUDED_)
#define AFX_MULTICLIPDLG_H__397C8831_AC75_43CC_AD72_68D514B9395B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CMultiClipDlg dialog

#include <FileText.h>
#include <LayeredWindow.h>

#include "Hotkey.h"
#include "ListClipboardContent.h"

#define WM_ICONNOTIFY							(WM_USER+1003)

#define CANCEL_CMD								40000
#define ADDSPECIALENTRY_CMD						40001
#define MANAGER_CMD								40002
#define HIDEWINDOW_CMD							40003


#define BASE_CLIPENTRY_CMD						41000
#define BASE_CLIPSPECIALENTRY_CMD				42000

#define BASE_CMD_LV_CLIPBOARD_CONTENT			43000
#define BASE_CMD_LV_SPECIAL_CLIPBOARD_CONTENT	44000

enum {
	LVC_CmdAddEntry = 10,
	LVC_CmdCopyEntry,
	LVC_CmdEditEntry,
	LVC_CmdDeleteEntry,
	LVC_CmdCopyToOther,
	LVC_CmdDeleteAllEntries,
	LVC_CmdLast
};



class CMultiClipDlg : public CDialog
{
	// Construction
public:
	void EnableControls();
	void TrayIconShow(BOOL fShow);
	void RestoreWindow();
	HWND m_hwndNextClipboardViewer;
	HWND m_hwndFocused;
	CMultiClipDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CMultiClipDlg)
	enum { IDD = IDD_MULTICLIP_DIALOG };
	CSliderCtrl	m_sliderTransparency;
	CListClipboardContent	m_lvSpecialClipboardContent;
	CListClipboardContent	m_lvClipboardContent;
	CString	m_sEntriesCount;
	CString	m_sSpecialEntriesCount;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMultiClipDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual BOOL OnCommand(WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CMultiClipDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	virtual void OnCancel();
	afx_msg void OnButtonQuit();
	afx_msg void OnDrawClipboard();
	afx_msg void OnChangeCbChain(HWND hWndRemove, HWND hWndAfter);
	afx_msg void OnItemchangedListClipContent(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonOptions();
	afx_msg void OnButtonAbout();
	afx_msg void OnDestroy();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnClose();
	afx_msg void OnDblclkListClipContent(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRclickListClipContent(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnCheckAlwaysOnTop();
	afx_msg void OnDblclkListSpecialClipContent(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRclickListSpecialClipContent(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg BOOL OnQueryEndSession();
	afx_msg void OnButtonHelp();
	afx_msg BOOL OnHelpInfo(HELPINFO* pHelpInfo);
	afx_msg void OnButtonRegisterUnregister();
	afx_msg void OnReleasedcaptureSliderTransparency(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	afx_msg LRESULT OnHotKey(WPARAM, LPARAM);
	afx_msg LRESULT OnIconNotify(WPARAM, LPARAM);
	DECLARE_MESSAGE_MAP()

public:
	BOOL m_fRegisteredViewer;
	BOOL UnRegisterViewer();
	BOOL RegisterViewer();
	void HideWindow();
	void RestoreSpecialEntries();
	void SaveSpecialEntries();
	void PasteToFocusWindowFromPopupMenu(CListClipboardContent& lv, int nIndex, BOOL fShiftInsert);
	void PasteToFocusWindow(BOOL fShiftInsert);
	void CopyClipboardToSpecialClipboard();
	void PasteFromSpecialBuf(CClipEntry& clipEntryBuf);
	void CopyClipboardToSpecialBuf(CClipEntry& clipEntryBuf, int nCtrlStaticId);
	void LVCommand(CListClipboardContent& lv, int nCommand);
	void AddEntry(CListClipboardContent& lv);
	void DeleteEntry(CListClipboardContent& lv, int nIndex);
	void CopyEntryToClipboard(CListClipboardContent& lv, int nIndex);
	void CopyEntryToOther(CListClipboardContent& lvDst, CListClipboardContent& lvSrc, int nIndex);
	void EditEntry(CListClipboardContent& lv, int nIndex);
	void PopupMenuListClipboardContent(CListCtrlEx2& lv, WPARAM wBaseCmd);
	void ShowPopupClipEntry();
	CString m_sTitle;
	void DeleteAllEntries(CListClipboardContent& lv);
	void AdjustClipEntries(CListClipboardContent& lv);
	UINT m_nMaxClipEntries;
	BOOL m_fNoMultipleEntry;
	static BOOL RestoreOptions(BOOL& fStartWithSession, BOOL& fNoMultipleEntry, UINT& nMaxClipEntries, DWORD& dwHotkeyPopupMenuCtrlV, DWORD& dwHotkeyPopupMenuShiftInsert, DWORD& dwHotkeyInsertSpecialClipboard, DWORD& dwHotkeyCopySpecialBuf1, DWORD& dwHotkeyPasteSpecialBuf1, DWORD& dwHotkeyCopySpecialBuf2, DWORD& dwHotkeyPasteSpecialBuf2);
	static BOOL SaveOptions(BOOL fStartWithSession, BOOL fNoMultipleEntry, UINT nMaxClipEntries, DWORD dwHotkeyPopupMenuCtrlV, DWORD dwHotkeyPopupMenuShiftInsert, DWORD dwHotkeyInsertSpecialClipboard, DWORD dwHotkeyCopySpecialBuf1, DWORD dwHotkeyPasteSpecialBuf1, DWORD dwHotkeyCopySpecialBuf2, DWORD dwHotkeyPasteSpecialBuf2);
	static BOOL RestoreOptionAlwaysOnTop(BOOL& fAlwaysOnTop);
	static BOOL SaveOptionAlwaysOnTop(BOOL fAlwaysOnTop);
	static BOOL RestoreOptionTransparency(BYTE& bTransparency);
	static BOOL SaveOptionTransparency(BYTE bTransparency);
	void FillClipboardContent();
	BOOL  m_fShiftInsert;

	CHotkey m_hkPopupMenuCtrlV;
	CHotkey m_hkPopupMenuShiftInsert;
	CHotkey m_hkInsertSpecialClipboard;

	CHotkey m_hkCopySpecialBuf1;
	CHotkey m_hkPasteSpecialBuf1;
	CHotkey m_hkCopySpecialBuf2;
	CHotkey m_hkPasteSpecialBuf2;

	CClipEntry m_clipEntryBuf1;
	CClipEntry m_clipEntryBuf2;

	CFileText m_ftLog;

	CLayeredWindow m_layeredWindow;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MULTICLIPDLG_H__397C8831_AC75_43CC_AD72_68D514B9395B__INCLUDED_)
