#pragma once

//-------------------------------------------
//
//	Logging system
//
//-------------------------------------------
//	CR/LF for static configuration
#define  S_LF	_T("\r\n")

#define _DT	//	No need to translate

#define LOGGER_BASE_PRODUCT_CLASS				LOGGER_BASE_CLASS _T(".MultiClip")

#define LOGGER_MULTICLIP_MAINDLG_CLASS			LOGGER_BASE_PRODUCT_CLASS ".MultiClipDlg"				

#define LOGGER_MULTICLIP_REGISTRY_CONFIG_CLASS	LOGGER_BASE_PRODUCT_CLASS ".RegistryConfig"

// **************************
//		Common
// **************************

//	Default appender
#define MULTICLIP_DEFAULT_APPENDER				_T("DefaultAppender")



CString GetModuleVersion();
CString GetAppVersion();
CString GetAppBuild();
CString GetAppBuildAndDate();
