// MultiClipDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MultiClip.h"
#include "MultiClipDlg.h"

#include "ClipContentDlg.h"
#include "Global.h"
#include "OptionsDlg.h"
#include "Vers.h"

#include <dateref.h>
#include <LibWin.h>
#include <Log4CppLib.h>
#include <macrosdef.h>
#include <registry.h>
#include <shortcut.h>
#include <splashdlg.h>
#include <winerr.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define CUR_TIME	CDateRef::GetCurrentDateTimeString()

#define HKEY_APPLICATION						HKEY_CURRENT_USER
#ifndef _DEBUG
#define STR_REG_APPLICATION						"Software\\FCSoftware\\MultiClip"
#else
#define STR_REG_APPLICATION						"Software\\FCSoftware\\MultiClip(D)"
#endif
#define STR_REG_ENTRY							"Entry.%03d"
#define STR_REG_ALWAYS_ON_TOP					"AlwaysOnTop"
#define STR_REG_SPECIAL_ENTRIES					"SpecialEntries"
#define STR_REG_MAX_CLIP_ENTRIES				"MaxClipEntries"
#define STR_REG_NO_MULTIPLE_ENTRY				"NoMultipleEntry"
#define STR_REG_HOTKEY_POPUP_MENU_CTRLV			"HotkeyPopupMenuCtrlV"
#define STR_REG_HOTKEY_POPUP_MENU_SHIFTINSERT	"HotkeyPopupMenuShiftInsert"
#define STR_REG_HOTKEY_INSERT_SPECIAL_CLIPBOARD	"HotkeyInsertSpecialClipboard"
#define STR_REG_HOTKEY_COPY_SPECIAL_BUF1		"HotkeyCopySpecialBuf1"
#define STR_REG_HOTKEY_PASTE_SPECIAL_BUF1		"HotkeyPasteSpecialBuf1"
#define STR_REG_HOTKEY_COPY_SPECIAL_BUF2		"HotkeyCopySpecialBuf2"
#define STR_REG_HOTKEY_PASTE_SPECIAL_BUF2		"HotkeyPasteSpecialBuf2"
#define STR_REG_TRANSPARENCY					"Transparency"

/////////////////////////////////////////////////////////////////////////////
// CMultiClipDlg dialog

CMultiClipDlg::CMultiClipDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMultiClipDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMultiClipDlg)
	m_sEntriesCount = _T("");
	m_sSpecialEntriesCount = _T("");
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_fNoMultipleEntry = false;
	m_hwndFocused = NULL;
	m_fRegisteredViewer = false;
}

void CMultiClipDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMultiClipDlg)
	DDX_Control(pDX, IDC_SLIDER_TRANSPARENCY, m_sliderTransparency);
	DDX_Control(pDX, IDC_LIST_SPECIAL_CLIP_CONTENT, m_lvSpecialClipboardContent);
	DDX_Control(pDX, IDC_LIST_CLIP_CONTENT, m_lvClipboardContent);
	DDX_Text(pDX, IDC_STATIC_ENTRIES_COUNT, m_sEntriesCount);
	DDX_Text(pDX, IDC_STATIC_SPECIAL_ENTRIES_COUNT, m_sSpecialEntriesCount);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CMultiClipDlg, CDialog)
	//{{AFX_MSG_MAP(CMultiClipDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_QUIT, OnButtonQuit)
	ON_WM_DRAWCLIPBOARD()
	ON_WM_CHANGECBCHAIN()
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_CLIP_CONTENT, OnItemchangedListClipContent)
	ON_BN_CLICKED(IDC_BUTTON_OPTIONS, OnButtonOptions)
	ON_BN_CLICKED(IDC_BUTTON_ABOUT, OnButtonAbout)
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_CLOSE()
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_CLIP_CONTENT, OnDblclkListClipContent)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_CLIP_CONTENT, OnRclickListClipContent)
	ON_BN_CLICKED(IDC_CHECK_ALWAYS_ON_TOP, OnCheckAlwaysOnTop)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_SPECIAL_CLIP_CONTENT, OnDblclkListSpecialClipContent)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_SPECIAL_CLIP_CONTENT, OnRclickListSpecialClipContent)
	ON_WM_TIMER()
	ON_WM_QUERYENDSESSION()
	ON_BN_CLICKED(IDC_BUTTON_HELP, OnButtonHelp)
	ON_WM_HELPINFO()
	ON_BN_CLICKED(IDC_BUTTON_REGISTER_UNREGISTER, OnButtonRegisterUnregister)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SLIDER_TRANSPARENCY, OnReleasedcaptureSliderTransparency)
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_ICONNOTIFY, OnIconNotify)
	ON_MESSAGE(WM_HOTKEY, OnHotKey)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMultiClipDlg message handlers

BOOL CMultiClipDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	//	Transparency
	m_layeredWindow.Enable(*this, true);

	//m_ftLog.OpenCreate(ConcatFileNameComposant(GetFileNameComposant(GetModuleFileNameStr(), GFNC_DIRECTORY), "MultiClip.log"), CFile::modeWrite | CFile::modeCreate  | CFile::shareDenyNone);
	m_ftLog.OpenCreate(ConcatFileNameComposant(GetEnv("TEMP"), "MultiClip.log"), CFile::modeWrite | CFile::modeCreate | CFile::shareDenyNone);
	SetWindowText(m_sTitle);
#if 0
#ifndef _DEBUG
	if (stricmp(GetComputerNameStr(), "pcfcolas") && stricmp(GetComputerNameStr(), "pcclepagn"))
		GetDlgItem(IDC_BUTTON_REGISTER_UNREGISTER)->ShowWindow(SW_HIDE);
#endif
#endif
	m_lvClipboardContent.Init();
	m_lvSpecialClipboardContent.Init();
	SetDlgItemText(IDC_STATIC_SPECIAL_BUF1, "");
	SetDlgItemText(IDC_STATIC_SPECIAL_BUF2, "");

	//	Restore special entries
	LDA("RestoreSpecialEntries");
	RestoreSpecialEntries();

	//	Register myself as clipboard viewer
	LDA("RestoreWindowPos");
	RestoreWindowPos(this, STR_REG_APPLICATION, HKEY_APPLICATION);

	BOOL fStartWithSession;
	DWORD dwHotkeyPopupMenuCtrlV, dwHotkeyPopupMenuShiftInsert, dwHotkeyInsertSpecialClipboard;
	DWORD dwHotkeyCopySpecialBuf1, dwHotkeyPasteSpecialBuf1, dwHotkeyCopySpecialBuf2, dwHotkeyPasteSpecialBuf2;
	LDA("RestoreOptions");
	RestoreOptions(fStartWithSession, m_fNoMultipleEntry, m_nMaxClipEntries, dwHotkeyPopupMenuCtrlV, dwHotkeyPopupMenuShiftInsert, dwHotkeyInsertSpecialClipboard, dwHotkeyCopySpecialBuf1, dwHotkeyPasteSpecialBuf1, dwHotkeyCopySpecialBuf2, dwHotkeyPasteSpecialBuf2);
	LDA("After RestoreOptions");

#define REGISTER_HK(_hk, _value)	\
	if(! _hk.RegisterHotkey(*this, _value)) \
	{ \
		CString sMsg; \
		sMsg.Format("!Erreur: enregistrement de la s�quence de touches '%s' impossible (%s)", _hk.GetHotkeyName(), GetLastErrorString()); \
		MessageBox(sMsg, "Multi Clipboard", MB_ICONSTOP); \
	}

	REGISTER_HK(m_hkPopupMenuCtrlV, dwHotkeyPopupMenuCtrlV);
	REGISTER_HK(m_hkPopupMenuShiftInsert, dwHotkeyPopupMenuShiftInsert);
	REGISTER_HK(m_hkInsertSpecialClipboard, dwHotkeyInsertSpecialClipboard);
	REGISTER_HK(m_hkCopySpecialBuf1, dwHotkeyCopySpecialBuf1);
	REGISTER_HK(m_hkPasteSpecialBuf1, dwHotkeyPasteSpecialBuf1);
	REGISTER_HK(m_hkCopySpecialBuf2, dwHotkeyCopySpecialBuf2);
	REGISTER_HK(m_hkPasteSpecialBuf2, dwHotkeyPasteSpecialBuf2);

	LDA("HotkeyPopupMenuCtrlV:%s", m_hkPopupMenuCtrlV.GetHotkeyName());
	LDA("HotkeyPopupMenuShiftInsert:%s", m_hkPopupMenuShiftInsert.GetHotkeyName());
	LDA("HotkeyInsertSpecialClip:%s", m_hkInsertSpecialClipboard.GetHotkeyName());
	LDA("HotkeyCopySpecialBuf1:%s", m_hkCopySpecialBuf1.GetHotkeyName());
	LDA("HotkeyPasteSpecialBuf1:%s", m_hkPasteSpecialBuf1.GetHotkeyName());
	LDA("HotkeyCopySpecialBuf2:%s", m_hkCopySpecialBuf2.GetHotkeyName());
	LDA("HotkeyPasteSpecialBuf2:%s", m_hkPasteSpecialBuf2.GetHotkeyName());

	//	Window top most
	BOOL fAlwaysOnTop;
	RestoreOptionAlwaysOnTop(fAlwaysOnTop);
	CheckDlgButton(IDC_CHECK_ALWAYS_ON_TOP, fAlwaysOnTop);
	SetWindowTopMost(this, fAlwaysOnTop);

	//	Tranparency
	m_sliderTransparency.SetRangeMin(0);
	m_sliderTransparency.SetRangeMax(80);
	BYTE bTransparency;
	RestoreOptionTransparency(bTransparency);
	m_sliderTransparency.SetPos(bTransparency);
	m_layeredWindow.SetTransparencyPercentage(bTransparency);

	TrayIconShow(TRUE);
	EnableControls();

#ifdef _DEBUG
	const char * szText;
	HANDLE h;
	/*
	szText = "Texte 1";
	h = CClipEntry::MakeHandle((void *)szText, strlen(szText)+1);
	m_lvClipboardContent.AddClipboardFormat(0, CF_TEXT, h, m_fNoMultipleEntry);
	szText = "Texte 2";
	h = CClipEntry::MakeHandle((void *)szText, strlen(szText)+1);
	m_lvClipboardContent.AddClipboardFormat(0, CF_TEXT, h, m_fNoMultipleEntry);
	szText = "Texte 3";
	h = CClipEntry::MakeHandle((void *)szText, strlen(szText)+1);
	m_lvClipboardContent.AddClipboardFormat(0, CF_TEXT, h, m_fNoMultipleEntry);
	szText = "Texte avec & apres and";
	h = CClipEntry::MakeHandle((void *)szText, strlen(szText)+1);
	m_lvClipboardContent.AddClipboardFormat(0, CF_TEXT, h, m_fNoMultipleEntry);
	szText = "Autre texte";
	h = CClipEntry::MakeHandle((void *)szText, strlen(szText)+1);
	m_lvClipboardContent.AddClipboardFormat(0, CF_TEXT, h, m_fNoMultipleEntry);
	szText = "Autre texte 1";
	h = CClipEntry::MakeHandle((void *)szText, strlen(szText)+1);
	m_lvClipboardContent.AddClipboardFormat(0, CF_TEXT, h, m_fNoMultipleEntry);
	szText = "Autre texte 2";
	h = CClipEntry::MakeHandle((void *)szText, strlen(szText)+1);
	m_lvClipboardContent.AddClipboardFormat(0, CF_TEXT, h, m_fNoMultipleEntry);
	szText = "Autre texte 3";
	h = CClipEntry::MakeHandle((void *)szText, strlen(szText)+1);
	m_lvClipboardContent.AddClipboardFormat(0, CF_TEXT, h, m_fNoMultipleEntry);
	*/
	szText = "Unless otherwise noted, the decimal numbers appearing in packet-";
	h = CClipEntry::MakeHandle((void *)szText, strlen(szText) + 1);
	m_lvClipboardContent.AddClipboardFormat(0, CF_TEXT, h, m_fNoMultipleEntry);
	szText = "Unless otherwise noted, the decimal numbers appearing in packet-";
	h = CClipEntry::MakeHandle((void *)szText, strlen(szText) + 1);
	m_lvClipboardContent.AddClipboardFormat(0, CF_TEXT, h, m_fNoMultipleEntry);
	szText = "There exists, also, a need for strong authentication of such";
	h = CClipEntry::MakeHandle((void *)szText, strlen(szText) + 1);
	m_lvClipboardContent.AddClipboardFormat(0, CF_TEXT, h, m_fNoMultipleEntry);
	szText = "The key contributors are: Marcus Leech: Bell-Northern Research,";
	h = CClipEntry::MakeHandle((void *)szText, strlen(szText) + 1);
	m_lvClipboardContent.AddClipboardFormat(0, CF_TEXT, h, m_fNoMultipleEntry);
	szText = "This document specifies an Internet standards track protocol ";
	h = CClipEntry::MakeHandle((void *)szText, strlen(szText) + 1);
	m_lvClipboardContent.AddClipboardFormat(0, CF_TEXT, h, m_fNoMultipleEntry);

#endif

	//m_hwndNextClipboardViewer = SetClipboardViewer();
	RegisterViewer();
#ifndef _DEBUG
	PostMessage(WM_COMMAND, HIDEWINDOW_CMD);
#endif

	//SetTimer(1, 10000, NULL);
	return TRUE;  // return TRUE  unless you set the focus to a control
}

BOOL CMultiClipDlg::OnQueryEndSession()
{
	if (!CDialog::OnQueryEndSession())
		return FALSE;

	// TODO: Add your specialized query end session code here
	SaveSpecialEntries();
	return TRUE;
}

void CMultiClipDlg::OnButtonQuit()
{
	// TODO: Add your control notification handler code here
	UnRegisterViewer();

	SaveSpecialEntries();

	LDA("Allocated handles:%d", CClipEntry::GetHandleAllocated());
	DeleteAllEntries(m_lvClipboardContent);
	LDA("Allocated handles:%d", CClipEntry::GetHandleAllocated());
	DeleteAllEntries(m_lvSpecialClipboardContent);
	LDA("Allocated handles:%d", CClipEntry::GetHandleAllocated());

	SaveWindowPos(this, STR_REG_APPLICATION, HKEY_APPLICATION);
	m_hkPopupMenuCtrlV.UnRegisterHotkey();
	m_hkPopupMenuShiftInsert.UnRegisterHotkey();
	m_hkInsertSpecialClipboard.UnRegisterHotkey();

	m_hkCopySpecialBuf1.UnRegisterHotkey();
	m_hkPasteSpecialBuf1.UnRegisterHotkey();
	m_hkCopySpecialBuf2.UnRegisterHotkey();
	m_hkPasteSpecialBuf2.UnRegisterHotkey();

	m_clipEntryBuf1.HandleFree();
	m_clipEntryBuf2.HandleFree();

	EndDialog(false);
}

void CMultiClipDlg::OnClose()
{
	// TODO: Add your message handler code here and/or call default
	OnButtonQuit();
	//CDialog::OnClose();
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMultiClipDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM)dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMultiClipDlg::OnQueryDragIcon()
{
	return (HCURSOR)m_hIcon;
}

void CMultiClipDlg::OnOK()
{
	// TODO: Add extra validation here

	//CDialog::OnOK();
}

void CMultiClipDlg::OnCancel()
{
	// TODO: Add extra cleanup here

	//CDialog::OnCancel();
#ifdef _DEBUG
	OnButtonQuit();
#else
	HideWindow();
#endif
}

void CMultiClipDlg::OnItemchangedListClipContent(NMHDR* pNMHDR, LRESULT* pResult)
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	EnableControls();
	*pResult = 0;
}

void CMultiClipDlg::OnButtonOptions()
{
	// TODO: Add your control notification handler code here
	COptionsDlg Dlg;

	RestoreOptions(Dlg.m_fStartWithSession, Dlg.m_fNoMultipleEntry, Dlg.m_nMaxClipEntries, Dlg.m_dwHotkeyPopupMenuCtrlV, Dlg.m_dwHotkeyPopupMenuShiftInsert, Dlg.m_dwHotkeyInsertSpecialClipboard, Dlg.m_dwHotkeyCopySpecialBuf1, Dlg.m_dwHotkeyPasteSpecialBuf1, Dlg.m_dwHotkeyCopySpecialBuf2, Dlg.m_dwHotkeyPasteSpecialBuf2);
	m_hkPopupMenuCtrlV.UnRegisterHotkey();
	m_hkPopupMenuShiftInsert.UnRegisterHotkey();
	m_hkInsertSpecialClipboard.UnRegisterHotkey();

	m_hkCopySpecialBuf1.UnRegisterHotkey();
	m_hkPasteSpecialBuf1.UnRegisterHotkey();
	m_hkCopySpecialBuf2.UnRegisterHotkey();
	m_hkPasteSpecialBuf2.UnRegisterHotkey();

	LDA("Avant-m_dwHotkeyInsertSpecialClipboard: %08lX", Dlg.m_dwHotkeyInsertSpecialClipboard);
	if (Dlg.DoModal() == IDOK)
	{
		SaveOptions(Dlg.m_fStartWithSession, Dlg.m_fNoMultipleEntry, Dlg.m_nMaxClipEntries, Dlg.m_dwHotkeyPopupMenuCtrlV, Dlg.m_dwHotkeyPopupMenuShiftInsert, Dlg.m_dwHotkeyInsertSpecialClipboard, Dlg.m_dwHotkeyCopySpecialBuf1, Dlg.m_dwHotkeyPasteSpecialBuf1, Dlg.m_dwHotkeyCopySpecialBuf2, Dlg.m_dwHotkeyPasteSpecialBuf2);
		LDA("HknamePopupMenuCtrlV:%s", CHotkey::GetHostkeyName(Dlg.m_dwHotkeyPopupMenuCtrlV));
		LDA("HknamePopupMenuShiftInsert:%s", CHotkey::GetHostkeyName(Dlg.m_dwHotkeyPopupMenuShiftInsert));
		LDA("HknameSpecialClipboard:%s", CHotkey::GetHostkeyName(Dlg.m_dwHotkeyInsertSpecialClipboard));
		LDA("HknameCopyBuf1:%s", CHotkey::GetHostkeyName(Dlg.m_dwHotkeyCopySpecialBuf1));
		LDA("HknamePasteBuf1:%s", CHotkey::GetHostkeyName(Dlg.m_dwHotkeyPasteSpecialBuf1));
		LDA("HknameCopyBuf2:%s", CHotkey::GetHostkeyName(Dlg.m_dwHotkeyCopySpecialBuf2));
		LDA("HknamePasteBuf2:%s", CHotkey::GetHostkeyName(Dlg.m_dwHotkeyPasteSpecialBuf2));

		m_nMaxClipEntries = Dlg.m_nMaxClipEntries;
		m_fNoMultipleEntry = Dlg.m_fNoMultipleEntry;
		AdjustClipEntries(m_lvClipboardContent);
		AdjustClipEntries(m_lvSpecialClipboardContent);
	}
	LDA("Apres-m_dwHotkeyInsertSpecialClipboard: %08lX", Dlg.m_dwHotkeyInsertSpecialClipboard);
	m_hkPopupMenuCtrlV.RegisterHotkey(*this, Dlg.m_dwHotkeyPopupMenuCtrlV);
	m_hkPopupMenuShiftInsert.RegisterHotkey(*this, Dlg.m_dwHotkeyPopupMenuShiftInsert);
	m_hkInsertSpecialClipboard.RegisterHotkey(*this, Dlg.m_dwHotkeyInsertSpecialClipboard);

	m_hkCopySpecialBuf1.RegisterHotkey(*this, Dlg.m_dwHotkeyCopySpecialBuf1);
	m_hkPasteSpecialBuf1.RegisterHotkey(*this, Dlg.m_dwHotkeyPasteSpecialBuf1);
	m_hkCopySpecialBuf2.RegisterHotkey(*this, Dlg.m_dwHotkeyCopySpecialBuf2);
	m_hkPasteSpecialBuf2.RegisterHotkey(*this, Dlg.m_dwHotkeyPasteSpecialBuf2);
}

void CMultiClipDlg::OnButtonAbout()
{
	// TODO: Add your control notification handler code here
	CString sVersion = GetAppVersion();
	CString sBuild = GetAppBuildAndDate();
	CString sAppliName;
	sAppliName.LoadString(IDS_MAIN_TITLE);
	CSplashDlg SD(this, sAppliName, sVersion, sBuild);
	SD.SetCenter();
	SD.SetIcon(AfxGetApp()->LoadIcon(MAKEINTRESOURCE(IDR_MAINFRAME)));
	SD.SetHistoResID(IDR_HISTO);
	SD.DoModal();
}


BOOL CMultiClipDlg::RegisterViewer()
{
	if (m_fRegisteredViewer) return false;
	LDA("RegisterViewer");
	m_hwndNextClipboardViewer = SetClipboardViewer();
	m_fRegisteredViewer = true;
	return true;
}

BOOL CMultiClipDlg::UnRegisterViewer()
{
	if (!m_fRegisteredViewer) return false;
	LDA("UnRegisterViewer");
	ChangeClipboardChain(m_hwndNextClipboardViewer);
	m_fRegisteredViewer = false;
	return true;
}

void CMultiClipDlg::OnDrawClipboard()
{
	CDialog::OnDrawClipboard();

	HWND hwndOwner = ::GetClipboardOwner();

	LDA("-------------------------------------------------------");
	m_ftLog.Printf("-------------------------------------------------\n");
	LDA("%s OnDrawClipboard checkthis=%d", CDateRef::GetCurrentDateTimeString(), hwndOwner == *this);
	m_ftLog.Printf("%s OnDrawClipboard checkthis=%d\n", CDateRef::GetCurrentDateTimeString(), hwndOwner == *this);
	char szWindowText[300], szClass[300];
	int nLenText = ::GetWindowText(hwndOwner, szWindowText, sizeof(szWindowText));
	int nLenClass = ::GetClassName(hwndOwner, szClass, sizeof(szClass));
	LDA("%s Owner: %08lX [%s/%s]", CUR_TIME, hwndOwner, nLenClass ? szClass : "[NoClass]", nLenText ? szWindowText : "[NoText]");
	m_ftLog.Printf("%s Owner: %08lX [%s/%s]\n", CUR_TIME, hwndOwner, nLenClass ? szClass : "[NoClass]", nLenText ? szWindowText : "[NoText]");

	//	Avoid copy of our own entries
	if (hwndOwner == *this) return;

	// TODO: Add your message handler code here
	if (OpenClipboard())
	{
		LDA("OnDrawClipboard ClipboardOpened");
		m_ftLog.Printf("OnDrawClipboard ClipboardOpened\n");
		CString sMsg;
		UINT uClipFormat = 0;
		for (;;)
		{
			uClipFormat = EnumClipboardFormats(uClipFormat);
			if (uClipFormat == 0)
			{
				if (GetLastError() != ERROR_SUCCESS)
				{
					sMsg.Format("Format error:%s", GetLastErrorString());
					//m_lbEvents.AddString(sMsg);
					LDAE("Format error:%s", GetLastErrorString());
					m_ftLog.Printf("Format error:%s\n", GetLastErrorString());
				}
				break;
			}
			else
			{
				//sMsg.Format("Format:%d", uClipFormat);
				if (uClipFormat == CF_TEXT)
				{
					HANDLE hData = GetClipboardData(uClipFormat);
					LDA("CT_TEXT hData=%08lX size:%d", hData, GlobalSize(hData));
					m_ftLog.Printf("CT_TEXT hData=%08lX size:%d\n", hData, GlobalSize(hData));
					if (GlobalSize(hData) > 0)
					{
						m_lvClipboardContent.AddClipboardFormat(0, uClipFormat, CClipEntry::CopyHandle(hData), m_fNoMultipleEntry);
						AdjustClipEntries(m_lvClipboardContent);
					}
				}
				else
				{
					HANDLE hData = GetClipboardData(uClipFormat);
					LDA("!= CF_TEXT hData=%08lX size:%d", hData, GlobalSize(hData));
					m_ftLog.Printf("!= CF_TEXT hData=%08lX size:%d\n", hData, GlobalSize(hData));
				}
			}
		}
		CloseClipboard();
	}
	else
	{
		LDA("OnDrawClipboard Unable to open Clipboard");
		m_ftLog.Printf("OnDrawClipboard Unable to open Clipboard\n");
	}
	::SendMessage(m_hwndNextClipboardViewer, WM_DRAWCLIPBOARD, NULL, NULL);
}

void CMultiClipDlg::OnChangeCbChain(HWND hWndRemove, HWND hWndAfter)
{
	CDialog::OnChangeCbChain(hWndRemove, hWndAfter);

	// TODO: Add your message handler code here
	char szWindow1[500], szWindow2[500];
	::GetWindowText(hWndRemove, szWindow1, sizeof(szWindow1));
	::GetWindowText(hWndRemove, szWindow2, sizeof(szWindow2));
	LDA("%s Change Clipboard Chain %s --> %s", CUR_TIME, szWindow1, szWindow2);
	m_ftLog.Printf("%s Change Clipboard Chain %s --> %s\n", CUR_TIME, szWindow1, szWindow2);
	if (m_hwndNextClipboardViewer == hWndRemove)
	{
		LDA("%s   Setting NewNextViewer %08lX %s", CUR_TIME, hWndAfter, szWindow2);
		m_ftLog.Printf("%s   Setting NewNextViewer %08lX %s\n", CUR_TIME, hWndAfter, szWindow2);
		m_hwndNextClipboardViewer = hWndAfter;
	}
	else if (m_hwndNextClipboardViewer)
	{
		::SendMessage(m_hwndNextClipboardViewer, WM_CHANGECBCHAIN, (WPARAM)hWndRemove, (LPARAM)hWndAfter);
	}
}

void CMultiClipDlg::OnDestroy()
{
	TrayIconShow(FALSE);

	CDialog::OnDestroy();

	// TODO: Add your message handler code here
}

void CMultiClipDlg::TrayIconShow(BOOL fShow)
{
	NOTIFYICONDATA NID;

	NID.cbSize = sizeof(NID);
	NID.hWnd = *this;
	NID.uID = 1;
	NID.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP;
	NID.uCallbackMessage = WM_ICONNOTIFY;
	NID.hIcon = m_hIcon;
	CString sTitle;
	GetWindowText(sTitle);
	strcpy(NID.szTip, sTitle);
	Shell_NotifyIcon(fShow ? NIM_ADD : NIM_DELETE, &NID);
}

LRESULT CMultiClipDlg::OnIconNotify(WPARAM, LPARAM lParam)
{
	if (NIM(lParam) == NIM_LBUTTONUP)
	{
		RestoreWindow();
	}
	return 0;
}

static BOOL s_fIn = false;

LRESULT CMultiClipDlg::OnHotKey(WPARAM wParam, LPARAM lParam)
{
	LDA("OnHotKey");
	UINT uIdHotKey = (UINT)wParam;			// identifier of hot key 
	UINT fuModifiers = (UINT)LOWORD(lParam);	// key-modifier flags 
	UINT uVirtKey = (UINT)HIWORD(lParam);	// virtual-key code

	if (m_hkPopupMenuCtrlV.GetHotkeyID() == uIdHotKey || m_hkPopupMenuShiftInsert.GetHotkeyID() == uIdHotKey)
	{
		HWND hwnd = ::GetForegroundWindow();
#ifdef _DEBUG
		char szName[200];
		::GetWindowText(hwnd, szName, sizeof(szName));
		LDA("Focus:%s", szName);
		//::BringWindowToTop(*this);
#endif
		m_fShiftInsert = m_hkPopupMenuShiftInsert.GetHotkeyID() == uIdHotKey;
		ShowPopupClipEntry();
		::SetForegroundWindow(hwnd);
		return 0;
	}
	else if (m_hkInsertSpecialClipboard.GetHotkeyID() == uIdHotKey)
	{
		LDA("InsertSpecialClipboard");
		CopyClipboardToSpecialClipboard();
	}
	else if (m_hkCopySpecialBuf1.GetHotkeyID() == uIdHotKey
		|| m_hkCopySpecialBuf2.GetHotkeyID() == uIdHotKey)
	{
		LDA("0-CopySpecialBuf %d", m_hkCopySpecialBuf1.GetHotkeyID() == uIdHotKey ? 1 : 2);
		CopyClipboardToSpecialBuf(m_hkCopySpecialBuf1.GetHotkeyID() == uIdHotKey ? m_clipEntryBuf1 : m_clipEntryBuf2, m_hkCopySpecialBuf1.GetHotkeyID() == uIdHotKey ? IDC_STATIC_SPECIAL_BUF1 : IDC_STATIC_SPECIAL_BUF2);
		LDA("1-CopySpecialBuf %d", m_hkCopySpecialBuf1.GetHotkeyID() == uIdHotKey ? 1 : 2);
	}
	else if (m_hkPasteSpecialBuf1.GetHotkeyID() == uIdHotKey
		|| m_hkPasteSpecialBuf2.GetHotkeyID() == uIdHotKey)
	{
		static DWORD s_dwCount = 0;
		if (!s_fIn)
		{
			s_fIn = true;
			s_dwCount++;
			LDA("0------------PasteSpecialBuf %d (%u)", m_hkPasteSpecialBuf1.GetHotkeyID() == uIdHotKey ? 1 : 2, s_dwCount);
			HWND hwnd = ::GetForegroundWindow();
#ifdef _DEBUG
			char szName[200];
			::GetWindowText(hwnd, szName, sizeof(szName));
			LDA("        Focus:%s", szName);
			//::BringWindowToTop(*this);
#endif
			PasteFromSpecialBuf(m_hkPasteSpecialBuf1.GetHotkeyID() == uIdHotKey ? m_clipEntryBuf1 : m_clipEntryBuf2);
			::SetForegroundWindow(hwnd);
			LDA("1------------PasteSpecialBuf %d", m_hkPasteSpecialBuf1.GetHotkeyID() == uIdHotKey ? 1 : 2);
			s_fIn = false;
		}
	}
	return 0;
}

void CMultiClipDlg::HideWindow()
{
	LDA("HideWindow");
	ShowWindow(SW_HIDE);
}

void CMultiClipDlg::RestoreWindow()
{
	ShowWindow(SW_RESTORE);
	SetForegroundWindow();
}

void CMultiClipDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialog::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	if (nType == SIZE_MINIMIZED) ShowWindow(SW_HIDE);
}

void CMultiClipDlg::EnableControls()
{
	UpdateData(true);
	m_sEntriesCount.Format("%d/%d entr�es", m_lvClipboardContent.GetItemCount(), m_nMaxClipEntries);
	m_sSpecialEntriesCount.Format("%d/%d entr�es", m_lvSpecialClipboardContent.GetItemCount(), m_nMaxClipEntries);
	UpdateData(false);
}

BOOL CMultiClipDlg::RestoreOptions(BOOL& fStartWithSession, BOOL& fNoMultipleEntry, UINT& nMaxClipEntries, DWORD& dwHotkeyPopupMenuCtrlV, DWORD& dwHotkeyPopupMenuShiftInsert, DWORD& dwHotkeyInsertSpecialClipboard, DWORD& dwHotkeyCopySpecialBuf1, DWORD& dwHotkeyPasteSpecialBuf1, DWORD& dwHotkeyCopySpecialBuf2, DWORD& dwHotkeyPasteSpecialBuf2)
{
	fStartWithSession = CShortcut::IsRegisteredInStartupMenu(GetModuleFileNameStr());
	nMaxClipEntries = 10;
	dwHotkeyPopupMenuCtrlV = CHotkey::MakeHotkey('V', MOD_ALT | MOD_SHIFT);
	dwHotkeyPopupMenuShiftInsert = CHotkey::MakeHotkey(VK_INSERT, MOD_ALT | MOD_SHIFT);
	dwHotkeyInsertSpecialClipboard = CHotkey::MakeHotkey(VK_INSERT, MOD_ALT | MOD_CONTROL);
	//	Special buf 1
	dwHotkeyCopySpecialBuf1 = CHotkey::MakeHotkey(VK_NUMPAD4, MOD_CONTROL);
	dwHotkeyPasteSpecialBuf1 = CHotkey::MakeHotkey(VK_NUMPAD1, MOD_CONTROL);
	//	Special buf 2
	dwHotkeyCopySpecialBuf2 = CHotkey::MakeHotkey(VK_NUMPAD5, MOD_CONTROL);
	dwHotkeyPasteSpecialBuf2 = CHotkey::MakeHotkey(VK_NUMPAD2, MOD_CONTROL);

	CRegistry reg(HKEY_APPLICATION, STR_REG_APPLICATION);
	if (!reg.Open(false)) return false;
	if (!reg.QueryValue(STR_REG_MAX_CLIP_ENTRIES, (unsigned long *)&nMaxClipEntries)) {}
	if (!reg.QueryValue(STR_REG_NO_MULTIPLE_ENTRY, &fNoMultipleEntry)) {}
	if (!reg.QueryValue(STR_REG_HOTKEY_POPUP_MENU_CTRLV, &dwHotkeyPopupMenuCtrlV)) {}
	if (!reg.QueryValue(STR_REG_HOTKEY_POPUP_MENU_SHIFTINSERT, &dwHotkeyPopupMenuShiftInsert)) {}
	if (!reg.QueryValue(STR_REG_HOTKEY_INSERT_SPECIAL_CLIPBOARD, &dwHotkeyInsertSpecialClipboard)) {}
	//	Special buf 1
	if (!reg.QueryValue(STR_REG_HOTKEY_COPY_SPECIAL_BUF1, &dwHotkeyCopySpecialBuf1)) {}
	if (!reg.QueryValue(STR_REG_HOTKEY_PASTE_SPECIAL_BUF1, &dwHotkeyPasteSpecialBuf1)) {}
	//	Special buf 2
	if (!reg.QueryValue(STR_REG_HOTKEY_COPY_SPECIAL_BUF2, &dwHotkeyCopySpecialBuf2)) {}
	if (!reg.QueryValue(STR_REG_HOTKEY_PASTE_SPECIAL_BUF2, &dwHotkeyPasteSpecialBuf2)) {}
	reg.Close();
	return true;
}

BOOL CMultiClipDlg::SaveOptions(BOOL fStartWithSession, BOOL fNoMultipleEntry, UINT nMaxClipEntries, DWORD dwHotkeyPopupMenuCtrlV, DWORD dwHotkeyPopupMenuShiftInsert, DWORD dwHotkeyInsertSpecialClipboard, DWORD dwHotkeyCopySpecialBuf1, DWORD dwHotkeyPasteSpecialBuf1, DWORD dwHotkeyCopySpecialBuf2, DWORD dwHotkeyPasteSpecialBuf2)
{
	CShortcut::RegisterInStartupMenu(GetModuleFileNameStr(), fStartWithSession);
	CRegistry reg(HKEY_APPLICATION, STR_REG_APPLICATION);
	if (!reg.Open(true)) return false;
	if (!reg.SetValue(STR_REG_MAX_CLIP_ENTRIES, (unsigned long)nMaxClipEntries)) {}
	if (!reg.SetValue(STR_REG_NO_MULTIPLE_ENTRY, fNoMultipleEntry)) {}
	if (!reg.SetValue(STR_REG_HOTKEY_POPUP_MENU_CTRLV, dwHotkeyPopupMenuCtrlV)) {}
	if (!reg.SetValue(STR_REG_HOTKEY_POPUP_MENU_SHIFTINSERT, dwHotkeyPopupMenuShiftInsert)) {}
	if (!reg.SetValue(STR_REG_HOTKEY_INSERT_SPECIAL_CLIPBOARD, dwHotkeyInsertSpecialClipboard)) {}
	//	Special buf 1
	if (!reg.SetValue(STR_REG_HOTKEY_COPY_SPECIAL_BUF1, dwHotkeyCopySpecialBuf1)) {}
	if (!reg.SetValue(STR_REG_HOTKEY_PASTE_SPECIAL_BUF1, dwHotkeyPasteSpecialBuf1)) {}
	//	Special buf 2
	if (!reg.SetValue(STR_REG_HOTKEY_COPY_SPECIAL_BUF2, dwHotkeyCopySpecialBuf2)) {}
	if (!reg.SetValue(STR_REG_HOTKEY_PASTE_SPECIAL_BUF2, dwHotkeyPasteSpecialBuf2)) {}
	reg.Close();
	return true;
}

BOOL CMultiClipDlg::RestoreOptionAlwaysOnTop(BOOL& fAlwaysOnTop)
{
	fAlwaysOnTop = false;
	CRegistry reg(HKEY_APPLICATION, STR_REG_APPLICATION);
	if (!reg.Open(false)) return false;
	if (!reg.QueryValue(STR_REG_ALWAYS_ON_TOP, &fAlwaysOnTop)) {}
	reg.Close();
	return true;
}

BOOL CMultiClipDlg::SaveOptionAlwaysOnTop(BOOL fAlwaysOnTop)
{
	CRegistry reg(HKEY_APPLICATION, STR_REG_APPLICATION);
	if (!reg.Open(true)) return false;
	if (!reg.SetValue(STR_REG_ALWAYS_ON_TOP, fAlwaysOnTop)) {}
	reg.Close();
	return true;
}

BOOL CMultiClipDlg::RestoreOptionTransparency(BYTE& bTransparency)
{
	bTransparency = 0;
	CRegistry reg(HKEY_APPLICATION, STR_REG_APPLICATION);
	if (!reg.Open(false)) return false;
	if (!reg.QueryValue(STR_REG_TRANSPARENCY, &bTransparency)) {}
	reg.Close();
	return true;
}

BOOL CMultiClipDlg::SaveOptionTransparency(BYTE bTransparency)
{
	CRegistry reg(HKEY_APPLICATION, STR_REG_APPLICATION);
	if (!reg.Open(true)) return false;
	if (!reg.SetValue(STR_REG_TRANSPARENCY, bTransparency)) {}
	reg.Close();
	return true;
}

void CMultiClipDlg::SaveSpecialEntries()
{
	CRegistry reg(HKEY_APPLICATION, STR_REG_APPLICATION "\\" STR_REG_SPECIAL_ENTRIES);
	if (!reg.Open(true)) return;
	reg.DeleteAllValues();
	HANDLE hData;
	DWORD dwSize;
	CString sEntry;
	LPTSTR lpData;
	for (int nIndex = 0; nIndex < m_lvSpecialClipboardContent.GetItemCount(); nIndex++)
	{
		hData = m_lvSpecialClipboardContent.GetDataHandle(nIndex);
		dwSize = GlobalSize(hData);
		lpData = (LPTSTR)CClipEntry::HandleLock(hData);

		sEntry.Format(STR_REG_ENTRY, nIndex);
		reg.SetValue(sEntry, lpData);

		CClipEntry::HandleUnlock(hData);
	}
}

void CMultiClipDlg::RestoreSpecialEntries()
{
	CRegistry reg(HKEY_APPLICATION, STR_REG_APPLICATION "\\" STR_REG_SPECIAL_ENTRIES);
	if (!reg.Open(false)) return;
	CString sEntry;
	char buf[5000];
	for (int nIndex = 0; nIndex < 200; nIndex++)
	{
		sEntry.Format(STR_REG_ENTRY, nIndex);
		if (reg.QueryValue(sEntry, buf, sizeof(buf) - 1))
		{
			buf[sizeof(buf) - 1] = '\0';
			m_lvSpecialClipboardContent.AddClipboardFormat(0, CF_TEXT,
				CClipEntry::MakeHandle((LPVOID)(const char *)buf, strlen(buf) + 1), m_fNoMultipleEntry);
		}
	}
}

BOOL CMultiClipDlg::OnCommand(WPARAM wParam, LPARAM lParam)
{
	// TODO: Add your specialized code here and/or call the base class
	LDA("OnCommand:%d", wParam);
	if (wParam == HIDEWINDOW_CMD)
	{
		HideWindow();
	}
	else if (wParam == CANCEL_CMD)
	{
		AttachThreadInput(GetWindowThreadProcessId(::GetForegroundWindow(), NULL), GetCurrentThreadId(), TRUE);
		AttachThreadInput(GetWindowThreadProcessId(::GetForegroundWindow(), NULL), GetCurrentThreadId(), FALSE);
	}
	else if (wParam == ADDSPECIALENTRY_CMD)
	{
		SetForegroundWindow();
		LVCommand(m_lvSpecialClipboardContent, LVC_CmdAddEntry);
	}
	else if (wParam == MANAGER_CMD)
	{
		SetForegroundWindow();
		RestoreWindow();
	}
	else if ((wParam >= BASE_CLIPENTRY_CMD) && (wParam <= (BASE_CLIPENTRY_CMD + 150)))
	{
		LDA(" Command: %d", wParam - BASE_CLIPENTRY_CMD);
		PasteToFocusWindowFromPopupMenu(m_lvClipboardContent, wParam - BASE_CLIPENTRY_CMD, m_fShiftInsert);
	}
	else if ((wParam >= BASE_CLIPSPECIALENTRY_CMD) && (wParam <= (BASE_CLIPSPECIALENTRY_CMD + 150)))
	{
		LDA(" Command: %d", wParam - BASE_CLIPSPECIALENTRY_CMD);
		PasteToFocusWindowFromPopupMenu(m_lvSpecialClipboardContent, wParam - BASE_CLIPSPECIALENTRY_CMD, m_fShiftInsert);
	}
	else if ((wParam >= BASE_CMD_LV_CLIPBOARD_CONTENT)
		&& (wParam <= (BASE_CMD_LV_CLIPBOARD_CONTENT + LVC_CmdLast)))
	{
		LVCommand(m_lvClipboardContent, wParam - BASE_CMD_LV_CLIPBOARD_CONTENT);
	}
	else if ((wParam >= BASE_CMD_LV_SPECIAL_CLIPBOARD_CONTENT)
		&& (wParam <= (BASE_CMD_LV_SPECIAL_CLIPBOARD_CONTENT + LVC_CmdLast)))
	{
		LVCommand(m_lvSpecialClipboardContent, wParam - BASE_CMD_LV_SPECIAL_CLIPBOARD_CONTENT);
	}
	return CDialog::OnCommand(wParam, lParam);
}

void CMultiClipDlg::LVCommand(CListClipboardContent& lv, int nCommand)
{
	switch (nCommand)
	{
	case LVC_CmdAddEntry:
		AddEntry(lv);
		break;
	case LVC_CmdCopyEntry:
		CopyEntryToClipboard(lv, lv.GetFirstSelectedItem());
		break;
	case LVC_CmdEditEntry:
		EditEntry(lv, lv.GetFirstSelectedItem());
		break;
	case LVC_CmdDeleteEntry:
		if (MessageBox("Etes vous sur de vouloir supprimer cette entr�e ?", "Confirmation", MB_YESNO | MB_ICONQUESTION) == IDYES)
		{
			DeleteEntry(lv, lv.GetFirstSelectedItem());
		}
		break;
	case LVC_CmdCopyToOther:
		if (lv == m_lvSpecialClipboardContent)
			CopyEntryToOther(m_lvClipboardContent, lv, lv.GetFirstSelectedItem());
		else
			CopyEntryToOther(m_lvSpecialClipboardContent, lv, lv.GetFirstSelectedItem());
		break;
	case LVC_CmdDeleteAllEntries:
		if (MessageBox("Etes vous sur de vouloir supprimer toutes les entr�es ?", "Multi Clipboard", MB_ICONQUESTION | MB_YESNO | MB_DEFBUTTON2) == IDYES)
		{
			DeleteAllEntries(lv);
		}
		break;
	default:
		ASSERT(false);
	}
}

#define CHECK_LV_INDEX(_lv,_index)	((_index >= 0) && (_index < _lv.GetItemCount()))

void CMultiClipDlg::AddEntry(CListClipboardContent& lv)
{
	CClipContentDlg Dlg;

	if (Dlg.DoModal() == IDOK)
	{
		int nIndex = lv.AddClipboardFormat(0, CF_TEXT,
			CClipEntry::MakeHandle((LPVOID)(const char *)Dlg.m_sClipContent, Dlg.m_sClipContent.GetLength() + 1), m_fNoMultipleEntry);
		lv.SetItemSel(nIndex);
	}
	EnableControls();
}

void CMultiClipDlg::DeleteEntry(CListClipboardContent& lv, int nIndex)
{
	if (!CHECK_LV_INDEX(lv, nIndex)) return;
	lv.DeleteItem(nIndex);
	EnableControls();
}

void CMultiClipDlg::CopyEntryToClipboard(CListClipboardContent& lv, int nIndex)
{
	if (!CHECK_LV_INDEX(lv, nIndex)) return;
	HANDLE hDatas = lv.GetDataHandle(nIndex);
	UINT uFormat = lv.GetFormatID(nIndex);
	CClipEntry::CopyToClipboard(*this, hDatas);
	m_lvClipboardContent.AddClipboardFormat(0, uFormat, hDatas, m_fNoMultipleEntry, true);
}

void CMultiClipDlg::EditEntry(CListClipboardContent &lv, int nIndex)
{
	if (!CHECK_LV_INDEX(lv, nIndex)) return;
	CClipContentDlg Dlg;

	HANDLE hDatas = lv.GetDataHandle(nIndex);
	LPTSTR lpDatas = (LPTSTR)CClipEntry::HandleLock(hDatas);
	if (lpDatas)
	{
		Dlg.m_sClipContent = lpDatas;
	}
	CClipEntry::HandleUnlock(hDatas);
	if (Dlg.DoModal() == IDOK)
	{
		UINT uClipFormat = lv.GetFormatID(nIndex);
		lv.DeleteItem(nIndex);
		lv.AddClipboardFormat(nIndex, uClipFormat,
			CClipEntry::MakeHandle((LPVOID)(const char *)Dlg.m_sClipContent, Dlg.m_sClipContent.GetLength() + 1), m_fNoMultipleEntry);
		lv.SetItemSel(nIndex);
	}
	EnableControls();
}

void CMultiClipDlg::CopyEntryToOther(CListClipboardContent& lvDst, CListClipboardContent& lvSrc, int nIndex)
{
	lvDst.AddClipboardFormat(0, lvSrc.GetFormatID(nIndex), CClipEntry::CopyHandle(lvSrc.GetDataHandle(nIndex)), m_fNoMultipleEntry);
	EnableControls();
}

void CMultiClipDlg::DeleteAllEntries(CListClipboardContent& lv)
{
	lv.DeleteAllItems();
	EnableControls();
}

void CMultiClipDlg::AdjustClipEntries(CListClipboardContent& lv)
{
	if ((UINT)lv.GetItemCount() > m_nMaxClipEntries)
	{
		for (int nIndex = lv.GetItemCount() - 1; nIndex >= (int)m_nMaxClipEntries; nIndex--)
		{
			lv.DeleteItem(nIndex);
		}
	}
	EnableControls();
}

void CMultiClipDlg::PopupMenuListClipboardContent(CListCtrlEx2& lv, WPARAM wBaseCmd)
{
	LDA("PopupMenuListClipboardContent");
	POINT PtScreen;
	GetCursorPos(&PtScreen);
	//PtScreen.x -= 5;

	CMenu PopupMenu;
	PopupMenu.CreatePopupMenu();

	if (lv.GetFirstSelectedItem() != -1)
	{
		PopupMenu.AppendMenu(MF_STRING | MF_BYCOMMAND, wBaseCmd + LVC_CmdCopyEntry, "&Copier cette entr�e dans le presse-papier");
		PopupMenu.AppendMenu(MF_STRING | MF_BYCOMMAND, wBaseCmd + LVC_CmdEditEntry, "&Editer cette entr�e...");
		PopupMenu.AppendMenu(MF_STRING | MF_BYCOMMAND, wBaseCmd + LVC_CmdDeleteEntry, "&Supprimer cette entr�e");
		PopupMenu.AppendMenu(MF_STRING | MF_BYCOMMAND, wBaseCmd + LVC_CmdDeleteAllEntries, "Supprimer &toutes les entr�es...");
		PopupMenu.AppendMenu(MF_SEPARATOR);
		PopupMenu.AppendMenu(MF_STRING | MF_BYCOMMAND, wBaseCmd + LVC_CmdCopyToOther, "Co&pier cette entr�e dans l'autre liste");
		PopupMenu.AppendMenu(MF_SEPARATOR);
	}

	PopupMenu.AppendMenu(MF_STRING | MF_BYCOMMAND, wBaseCmd + LVC_CmdAddEntry, "&Ajouter une entr�e...");
	PopupMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_RIGHTBUTTON, PtScreen.x, PtScreen.y, this);
}


void CMultiClipDlg::PasteToFocusWindowFromPopupMenu(CListClipboardContent& lv, int nIndex, BOOL fShiftInsert)
{
	CopyEntryToClipboard(lv, nIndex);
	PasteToFocusWindow(fShiftInsert);
}

void CMultiClipDlg::PasteToFocusWindow(BOOL fShiftInsert)
{
	//	Copy specified entry to clipboard

	LDA("0-PasteToFocusWindow AttachThreadInput,ShiftInsert:%d", fShiftInsert);
	AttachThreadInput(GetWindowThreadProcessId(::GetForegroundWindow(), NULL), GetCurrentThreadId(), TRUE);
	if (!fShiftInsert)
	{
		//	Send Ctrl+V to focused window
		keybd_event(VK_CONTROL, 0, 0, 0);
		keybd_event('V', 0, 0, 0);
		Sleep(10);
		keybd_event('V', 0, KEYEVENTF_KEYUP, 0);
		keybd_event(VK_CONTROL, 0, KEYEVENTF_KEYUP, 0);
	}
	else
	{
		keybd_event(VK_SHIFT, MapVirtualKey(VK_SHIFT, 0), 0, 0);
		keybd_event(VK_INSERT, MapVirtualKey(VK_INSERT, 0), KEYEVENTF_EXTENDEDKEY, 0);
		Sleep(10);
		keybd_event(VK_INSERT, MapVirtualKey(VK_INSERT, 0), KEYEVENTF_KEYUP, 0);
		keybd_event(VK_SHIFT, MapVirtualKey(VK_SHIFT, 0), KEYEVENTF_KEYUP, 0);
		/*
		::PostMessage(m_hwndFocused, WM_KEYDOWN, VK_SHIFT,
			MAKELONG(MAKEWORD(1,0),MAKEWORD(MapVirtualKey(VK_SHIFT, 0),0)));
		::PostMessage(m_hwndFocused, WM_KEYDOWN, VK_INSERT,
			MAKELONG(MAKEWORD(1,0),MAKEWORD(MapVirtualKey(VK_INSERT, 0),0x01)));
		Sleep(100);
		::PostMessage(m_hwndFocused, WM_KEYUP, VK_INSERT,
			MAKELONG(MAKEWORD(1,0),MAKEWORD(MapVirtualKey(VK_INSERT, 0),0xC1)));
		::PostMessage(m_hwndFocused, WM_KEYUP, VK_SHIFT,
			MAKELONG(MAKEWORD(1,0),MAKEWORD(MapVirtualKey(VK_SHIFT, 0),0xC0)));
		*/
	}
	LDA("1-PasteToFocusWindow AttachThreadInput,ShiftInsert:%d", fShiftInsert);
	AttachThreadInput(GetWindowThreadProcessId(::GetForegroundWindow(), NULL), GetCurrentThreadId(), FALSE);
}

void CMultiClipDlg::ShowPopupClipEntry()
{
	CMenu PopupMenu;
	PopupMenu.CreatePopupMenu();

	//	Entries
	char szDatas[60];
	HANDLE hData;
	DWORD dwSize;
	LPTSTR lpData;
	CString sText;
	char cIndex;
	for (int nIndex = 0; nIndex < m_lvClipboardContent.GetItemCount(); nIndex++)
	{
		if (m_lvClipboardContent.GetFormatID(nIndex) == CF_TEXT)
		{
			szDatas[0] = 0;
			hData = m_lvClipboardContent.GetDataHandle(nIndex);
			dwSize = GlobalSize(hData);
			lpData = (LPTSTR)CClipEntry::HandleLock(hData);
			CClipEntry::ConvertDatasToText(szDatas, sizeof(szDatas), lpData, dwSize, true);
			CClipEntry::HandleUnlock(hData);
			if (nIndex < 9) cIndex = nIndex + '1';
			else cIndex = nIndex + 'A' - 9;
			sText.Format("&%c. %s", cIndex, szDatas);
			PopupMenu.AppendMenu(MF_STRING | MF_BYCOMMAND, BASE_CLIPENTRY_CMD + nIndex, sText);
		}
	}

	//	Special entries
	PopupMenu.AppendMenu(MF_SEPARATOR);
	CMenu SpecialMenu;
	SpecialMenu.CreatePopupMenu();
	for (int nIndex = 0; nIndex < m_lvSpecialClipboardContent.GetItemCount(); nIndex++)
	{
		if (m_lvSpecialClipboardContent.GetFormatID(nIndex) == CF_TEXT)
		{
			szDatas[0] = 0;
			hData = m_lvSpecialClipboardContent.GetDataHandle(nIndex);
			dwSize = GlobalSize(hData);
			lpData = (LPTSTR)CClipEntry::HandleLock(hData);
			CClipEntry::ConvertDatasToText(szDatas, sizeof(szDatas), lpData, dwSize, true);
			CClipEntry::HandleUnlock(hData);
			if (nIndex < 9) cIndex = nIndex + '1';
			else cIndex = nIndex + 'A' - 9;
			sText.Format("&%c. %s", cIndex, szDatas);
			SpecialMenu.AppendMenu(MF_STRING | MF_BYCOMMAND, BASE_CLIPSPECIALENTRY_CMD + nIndex, sText);
		}
	}
	PopupMenu.AppendMenu(MF_POPUP, (UINT)SpecialMenu.m_hMenu, "&+. Entr�es sp�ciales");

	//	Add special entry
	PopupMenu.AppendMenu(MF_SEPARATOR);
	PopupMenu.AppendMenu(MF_STRING | MF_BYCOMMAND, ADDSPECIALENTRY_CMD, "&-. Ajouter une entr�e sp�ciale...");
	//	Options
	PopupMenu.AppendMenu(MF_SEPARATOR);
	PopupMenu.AppendMenu(MF_STRING | MF_BYCOMMAND, IDC_BUTTON_OPTIONS, "&/. Pr�f�rences...");
	//	Manager
	PopupMenu.AppendMenu(MF_SEPARATOR);
	PopupMenu.AppendMenu(MF_STRING | MF_BYCOMMAND, MANAGER_CMD, "&*. Gestionnaire...");
	//	Cancel
	PopupMenu.AppendMenu(MF_SEPARATOR);
	PopupMenu.AppendMenu(MF_STRING | MF_BYCOMMAND, CANCEL_CMD, "&0. Annuler");

	LDA("ShowPopupClipEntry AttachThreadInput, TRUE");
	AttachThreadInput(GetWindowThreadProcessId(::GetForegroundWindow(), NULL), GetCurrentThreadId(), TRUE);
	m_hwndFocused = ::GetFocus();
	LDA("ShowPopupClipEntry AttachThreadInput, FALSE");
	AttachThreadInput(GetWindowThreadProcessId(::GetForegroundWindow(), NULL), GetCurrentThreadId(), FALSE);
	CRect rect;
	::GetWindowRect(m_hwndFocused, &rect);
	LDA("Rect:[%d,%d]-[%d,%d]", rect.left, rect.top, rect.right, rect.bottom);
	rect.OffsetRect(20, 20);

	LDA("ShowPopupClipEntry SetForegroundWindow");
	SetForegroundWindow();

	PopupMenu.TrackPopupMenu(TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_RIGHTBUTTON, rect.left, rect.top, this);

	LDA("ShowPopupClipEntry SetForegroundWindow(focused)");
	::SetForegroundWindow(m_hwndFocused);
}

void CMultiClipDlg::OnDblclkListClipContent(NMHDR* pNMHDR, LRESULT* pResult)
{
	// TODO: Add your control notification handler code here
	CopyEntryToClipboard(m_lvClipboardContent, m_lvClipboardContent.GetFirstSelectedItem());
	m_lvClipboardContent.SelectItem(0);
	*pResult = 0;
}

void CMultiClipDlg::OnRclickListClipContent(NMHDR* pNMHDR, LRESULT* pResult)
{
	// TODO: Add your control notification handler code here
	LDA("OnRclickListClipContent");
	PopupMenuListClipboardContent(m_lvClipboardContent, BASE_CMD_LV_CLIPBOARD_CONTENT);
	*pResult = 0;
}

void CMultiClipDlg::OnDblclkListSpecialClipContent(NMHDR* pNMHDR, LRESULT* pResult)
{
	// TODO: Add your control notification handler code here
	CopyEntryToClipboard(m_lvSpecialClipboardContent, m_lvSpecialClipboardContent.GetFirstSelectedItem());
	*pResult = 0;
}

void CMultiClipDlg::OnRclickListSpecialClipContent(NMHDR* pNMHDR, LRESULT* pResult)
{
	// TODO: Add your control notification handler code here
	PopupMenuListClipboardContent(m_lvSpecialClipboardContent, BASE_CMD_LV_SPECIAL_CLIPBOARD_CONTENT);
	*pResult = 0;
}

void CMultiClipDlg::OnCheckAlwaysOnTop()
{
	// TODO: Add your control notification handler code here
	BOOL fIsAlwaysOnTop = IsDlgButtonChecked(IDC_CHECK_ALWAYS_ON_TOP);
	LDA("AlwaysOnTop=%d", fIsAlwaysOnTop);
	SaveOptionAlwaysOnTop(fIsAlwaysOnTop);
	SetWindowTopMost(this, fIsAlwaysOnTop);
}

void CMultiClipDlg::CopyClipboardToSpecialClipboard()
{
	if (OpenClipboard())
	{
		UINT uClipFormat = 0;
		for (;;)
		{
			uClipFormat = EnumClipboardFormats(uClipFormat);
			if (uClipFormat == 0)
			{
				if (GetLastError() != ERROR_SUCCESS)
				{
				}
				break;
			}
			else
			{
				//sMsg.Format("Format:%d", uClipFormat);
				if (uClipFormat == CF_TEXT)
				{
					HANDLE hData = GetClipboardData(uClipFormat);
					if (GlobalSize(hData) > 0)
					{
						m_lvSpecialClipboardContent.AddClipboardFormat(0, uClipFormat, CClipEntry::CopyHandle(hData), m_fNoMultipleEntry);
						AdjustClipEntries(m_lvSpecialClipboardContent);
					}
					else
					{
						LDA("GlobalSize == 0");
					}
				}
			}
		}
		CloseClipboard();
	}
}

void CMultiClipDlg::PasteFromSpecialBuf(CClipEntry& clipEntryBuf)
{
	if (clipEntryBuf.GetHandle())
	{
		CClipEntry::CopyToClipboard(*this, clipEntryBuf.GetHandle());
		PasteToFocusWindow(false);
	}
}

void CMultiClipDlg::CopyClipboardToSpecialBuf(CClipEntry& clipEntryBuf, int nCtrlStaticId)
{
	if (OpenClipboard())
	{
		UINT uClipFormat = 0;
		for (;;)
		{
			uClipFormat = EnumClipboardFormats(uClipFormat);
			if (uClipFormat == 0)
			{
				if (GetLastError() != ERROR_SUCCESS)
				{
				}
				break;
			}
			else
			{
				if (uClipFormat == CF_TEXT)
				{
					HANDLE hData = GetClipboardData(uClipFormat);
					if (GlobalSize(hData) > 0)
					{
						clipEntryBuf.SetHandleAndFormat(CClipEntry::CopyHandle(hData), uClipFormat);
						DWORD dwSize = CClipEntry::HandleSize(hData);
						LPCTSTR szText = (LPCTSTR)GlobalLock(hData);
						LDA("   ClipboardContent(%u bytes):%s", dwSize, szText);
						TCHAR szDatas[60];
						CClipEntry::ConvertDatasToText(szDatas, sizeof(szDatas), szText, dwSize);
						SetDlgItemText(nCtrlStaticId, szDatas);
						GlobalUnlock(hData);
					}
					else
					{
						LDA("GlobalSize == 0");
					}
				}
			}
		}
		CloseClipboard();
	}
}

void CMultiClipDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: Add your message handler code here and/or call default
	ChangeClipboardChain(m_hwndNextClipboardViewer);
	m_hwndNextClipboardViewer = SetClipboardViewer();
	CDialog::OnTimer(nIDEvent);
}

void CMultiClipDlg::OnButtonHelp()
{
	// TODO: Add your control notification handler code here
	CString sHelpFile;
	sHelpFile = ConcatFileNameComposant(
		GetFileNameComposant(GetModuleFileNameStr(), GFNC_DIRECTORY),
		GetFileNameComposant(GetModuleFileNameStr(), GFNC_FILENAME),
		"chm");
	if (ShellExecute(*this, "open", sHelpFile, NULL, NULL, SW_SHOWNORMAL) < (HANDLE)32)
	{
		MessageBox("Fichier d'aide non trouv�", "Erreur", MB_ICONINFORMATION);
	}
}

BOOL CMultiClipDlg::OnHelpInfo(HELPINFO* pHelpInfo)
{
	// TODO: Add your message handler code here and/or call default
	OnButtonHelp();
	return TRUE;
}

void CMultiClipDlg::OnButtonRegisterUnregister()
{
	// TODO: Add your control notification handler code here
	UnRegisterViewer();
	RegisterViewer();
}

void CMultiClipDlg::OnReleasedcaptureSliderTransparency(NMHDR* pNMHDR, LRESULT* pResult)
{
	// TODO: Add your control notification handler code here
	LDA("OnReleasedcaptureSliderTransparency");
	int nMin, nMax;
	m_sliderTransparency.GetSelection(nMin, nMax);

	LDA("PageSize:%d min:%d max:%d pos:%d", m_sliderTransparency.GetPageSize(), nMin, nMax, m_sliderTransparency.GetPos());
	BYTE bTransparency = m_sliderTransparency.GetPos();
	m_layeredWindow.SetTransparencyPercentage(bTransparency);
	SaveOptionTransparency(bTransparency);
	*pResult = 0;
}
