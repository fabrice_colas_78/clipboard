// ListClipboardContent.cpp : implementation file
//

#include "stdafx.h"
#include "MultiClip.h"
#include "ListClipboardContent.h"

#include <Log4CppLib.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CListClipboardContent

CListClipboardContent::CListClipboardContent()
{
}

CListClipboardContent::~CListClipboardContent()
{
}


BEGIN_MESSAGE_MAP(CListClipboardContent, CListCtrlEx2)
	//{{AFX_MSG_MAP(CListClipboardContent)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CListClipboardContent message handlers

void CListClipboardContent::Init()
{
	SetHighlightType(HIGHLIGHT_ALLCOLUMNS);
	ShowSelAlways(true);
	EnableSort(true);

	InsertColumn(CI_SIZE, "Taille", LVCFMT_RIGHT, 50);
	InsertColumn(CI_COUNT, "Occurences", LVCFMT_RIGHT, 40);
	InsertColumn(CI_DATA, "Donn�es", LVCFMT_LEFT, 1500);
	InsertColumn(CI_FORMAT, "Format", LVCFMT_LEFT, 80);
	InsertColumn(CI_FORMATID, "FormatID", LVCFMT_RIGHT,
#ifdef _DEBUG
		40);
#else
		0);
#endif
}

void CListClipboardContent::DeleteItem(int nIndex)
{
	HANDLE hData = GetDataHandle(nIndex);
	CClipEntry::HandleFree(hData);
	CClipEntry * pClipEntry = GetClipEntry(nIndex);
	if (pClipEntry) delete pClipEntry;
	CListCtrlEx2::DeleteItem(nIndex);
}

void CListClipboardContent::DeleteAllItems()
{
	for (int nIndex = GetItemCount() - 1; nIndex >= 0; nIndex--)
	{
		DeleteItem(nIndex);
	}
}

BOOL CListClipboardContent::IsClipboardEntryExist(UINT uFormat, HANDLE hDatas)
{
	for (int nIndex = 0; nIndex < GetItemCount(); nIndex++)
	{
		if (CClipEntry::CompareHandle(GetDataHandle(nIndex), hDatas)) return nIndex;
	}
	return -1;
}

int CListClipboardContent::AddClipboardFormat(int nIndex, UINT uFormat, HANDLE hDatas, BOOL fNoMultipleEntry, BOOL fDoNotFreeHandle)
{
	LDA("-----------------------");
	int nCount = 1;
	if (fNoMultipleEntry)
	{
		int nExistingIndex = IsClipboardEntryExist(uFormat, hDatas);
		LDA("NoMultipleEntry index=%d", nExistingIndex);
		if (nExistingIndex != -1)
		{
			if (!fDoNotFreeHandle)
			{
				//	Delete new data handle
				CClipEntry::HandleFree(hDatas);
				//	Increment existing counter
				IncCount(nExistingIndex);
			}
			//	Get current counter
			nCount = GetCount(nExistingIndex);
			//	Get data handle
			hDatas = GetDataHandle(nExistingIndex);
			//	free old CLipEntry
			CClipEntry * pClipEntry = GetClipEntry(nExistingIndex);
			delete pClipEntry;
			//	delete old entry
			CListCtrlEx2::DeleteItem(nExistingIndex);
			//return nExistingIndex;
		}
	}
	LDA("-- Count:%u", nCount);

	DWORD dwSize = CClipEntry::HandleSize(hDatas);
	LPTSTR lpData = (LPTSTR)CClipEntry::HandleLock(hDatas);

	CString s;
	s.Format("%u", dwSize);
	nIndex = InsertItem(nIndex, s);
	ASSERT(nIndex == nIndex);

	SetCount(nIndex, nCount);

	s.Format("%u", uFormat);
	SetItemText(nIndex, CI_FORMATID, s);

#ifdef _DEBUG
	char szDatas[40];
#else
	char szDatas[250];
#endif
	szDatas[0] = 0;

	switch (uFormat)
	{
	case CF_TEXT:
		s.Format("Text");
		CClipEntry::ConvertDatasToText(szDatas, sizeof(szDatas), lpData, dwSize);
		break;
	case CF_BITMAP:
		s.Format("Bitmap");
		CClipEntry::ConvertDatasToBCD(szDatas, sizeof(szDatas), lpData, dwSize);
		break;
	case CF_METAFILEPICT:
		s.Format("Metafile Picture");
		CClipEntry::ConvertDatasToBCD(szDatas, sizeof(szDatas), lpData, dwSize);
		break;
	case CF_SYLK:
		s.Format("Sylk");
		CClipEntry::ConvertDatasToBCD(szDatas, sizeof(szDatas), lpData, dwSize);
		break;
	case CF_DIF:
		s.Format("Dif");
		CClipEntry::ConvertDatasToBCD(szDatas, sizeof(szDatas), lpData, dwSize);
		break;
	case CF_TIFF:
		s.Format("Tiff");
		CClipEntry::ConvertDatasToBCD(szDatas, sizeof(szDatas), lpData, dwSize);
		break;
	case CF_OEMTEXT:
		s.Format("OEM Text");
		CClipEntry::ConvertDatasToText(szDatas, sizeof(szDatas), lpData, dwSize);
		break;
	case CF_DIB:
		s.Format("DIB");
		CClipEntry::ConvertDatasToBCD(szDatas, sizeof(szDatas), lpData, dwSize);
		break;
	case CF_PALETTE:
		s.Format("Palette");
		CClipEntry::ConvertDatasToBCD(szDatas, sizeof(szDatas), lpData, dwSize);
		break;
	case CF_PENDATA:
		s.Format("PenData");
		CClipEntry::ConvertDatasToBCD(szDatas, sizeof(szDatas), lpData, dwSize);
		break;
	case CF_RIFF:
		s.Format("Riff");
		CClipEntry::ConvertDatasToBCD(szDatas, sizeof(szDatas), lpData, dwSize);
		break;
	case CF_WAVE:
		s.Format("Wave");
		CClipEntry::ConvertDatasToBCD(szDatas, sizeof(szDatas), lpData, dwSize);
		break;
	case CF_UNICODETEXT:
		s.Format("Unicode Text");
		CClipEntry::ConvertDatasToBCD(szDatas, sizeof(szDatas), lpData, dwSize);
		break;
	case CF_ENHMETAFILE:
		s.Format("Enhanced metafile");
		CClipEntry::ConvertDatasToBCD(szDatas, sizeof(szDatas), lpData, dwSize);
		break;
	case CF_HDROP:
		s.Format("HDrop");
		CClipEntry::ConvertDatasToBCD(szDatas, sizeof(szDatas), lpData, dwSize);
		break;
	case CF_LOCALE:
		s.Format("Locale");
		CClipEntry::ConvertDatasToBCD(szDatas, sizeof(szDatas), lpData, dwSize);
		break;
	default:
	{
		char szFormatName[500];
		if (!GetClipboardFormatName(uFormat, szFormatName, sizeof(szFormatName)))
		{
			s.Format("[Unknown] %d", uFormat);
		}
		else
		{
			s = szFormatName;
		}
		CClipEntry::ConvertDatasToBCD(szDatas, sizeof(szDatas), lpData, dwSize);
	}
	break;
	}
	SetItemText(nIndex, CI_DATA, szDatas);
	SetItemText(nIndex, CI_FORMAT, s);

	CClipEntry::HandleUnlock(hDatas);

	CClipEntry * pClipEntry = new CClipEntry(hDatas, uFormat);
	SetClipEntry(nIndex, pClipEntry);
	return nIndex;
}
/*
BOOL CListClipboardContent::SetClipboardFormat(int nIndex, UINT uFormat, HANDLE hDatas)
{
	return true;
}*/

HANDLE CListClipboardContent::GetDataHandle(int nIndex)
{
	if ((nIndex < 0) || (nIndex >= GetItemCount())) return NULL;
	CClipEntry * pClipEntry = GetClipEntry(nIndex);
	if (!pClipEntry)
	{
		//ASSERT(FALSE);
		return NULL;
	}
	return pClipEntry->GetHandle();
}

HANDLE CListClipboardContent::GetCurrentDataHandle()
{
	return GetDataHandle(GetFirstSelectedItem());
}

UINT CListClipboardContent::GetFormatID(int nIndex)
{
	if ((nIndex < 0) || (nIndex >= GetItemCount())) return 0;
	CClipEntry * pClipEntry = GetClipEntry(nIndex);
	if (!pClipEntry)
	{
		ASSERT(FALSE);
		return NULL;
	}
	return pClipEntry->GetFormat();
}

UINT CListClipboardContent::GetCurrentFormatID()
{
	return GetFormatID(GetFirstSelectedItem());
}

CClipEntry * CListClipboardContent::GetClipEntry(int nIndex)
{
	return (CClipEntry *)GetItemData(nIndex);
}

BOOL CListClipboardContent::SetClipEntry(int nIndex, CClipEntry *pClipEntry)
{
	return SetItemData(nIndex, (LONG)pClipEntry);
}

int CListClipboardContent::GetCount(int nIndex)
{
	if ((nIndex < 0) || (nIndex >= GetItemCount())) return 0;
	CString s;
	s = GetItemText(nIndex, CI_COUNT);
	return atoi(s);
}

void CListClipboardContent::SetCount(int nIndex, int nCount)
{
	if ((nIndex < 0) || (nIndex >= GetItemCount())) return;
	CString s;
	s.Format("%d", nCount);
	SetItemText(nIndex, CI_COUNT, s);
}

void CListClipboardContent::IncCount(int nIndex)
{
	SetCount(nIndex, GetCount(nIndex) + 1);
}
