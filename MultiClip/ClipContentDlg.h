#if !defined(AFX_CLIPCONTENTDLG_H__85730619_642C_48A4_9493_353BE64B6087__INCLUDED_)
#define AFX_CLIPCONTENTDLG_H__85730619_642C_48A4_9493_353BE64B6087__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ClipContentDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CClipContentDlg dialog

class CClipContentDlg : public CDialog
{
	// Construction
public:
	CClipContentDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CClipContentDlg)
	enum { IDD = IDD_DIALOG_CLIP_CONTENT };
	CEdit	m_ebClipContent;
	CString	m_sClipContent;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CClipContentDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CClipContentDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLIPCONTENTDLG_H__85730619_642C_48A4_9493_353BE64B6087__INCLUDED_)
