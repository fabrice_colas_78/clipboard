// Hotkey.cpp: implementation of the CHotkey class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MultiClip.h"
#include "Hotkey.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//	Static functions
UINT CHotkey::GetFirstFreeHotkeyId()
{
	static int s_nHotkeyId = 10;
	return ++s_nHotkeyId;
}


WORD CHotkey::GetHotkeyModifiers(DWORD dwVirtualKeyAndModifiers)
{
	return HIWORD(dwVirtualKeyAndModifiers);
}

WORD CHotkey::ConvertModifiersToCtrl(WORD wModifiers)
{
	WORD wModifiersCtrl = 0;
	if (wModifiers & MOD_ALT)
		wModifiersCtrl |= HOTKEYF_ALT;

	if (wModifiers & MOD_CONTROL)
		wModifiersCtrl |= HOTKEYF_CONTROL;

	if (wModifiers & MOD_SHIFT)
		wModifiersCtrl |= HOTKEYF_SHIFT;

	/*if(wModifiers & MOD_WIN)
		wModifiersCtrl |= HOTKEYF_EXT;*/
	return wModifiersCtrl;
}

WORD CHotkey::ConvertModifiersFromCtrl(WORD wModifiersCtrl)
{
	WORD wModifiers = 0;
	if (wModifiersCtrl & HOTKEYF_ALT)
		wModifiers |= MOD_ALT;

	if (wModifiersCtrl & HOTKEYF_CONTROL)
		wModifiers |= MOD_CONTROL;

	if (wModifiersCtrl & HOTKEYF_SHIFT)
		wModifiers |= MOD_SHIFT;

	/*if(wModifiersCtrl & HOTKEYF_EXT)
		wModifiers |= MOD_WIN;*/
	return wModifiers;
}

WORD CHotkey::GetHotkeyVirtualKeyCode(DWORD dwVirtualKeyAndModifiers)
{
	return LOWORD(dwVirtualKeyAndModifiers);
}

DWORD CHotkey::ConvertHotkeyFromCtrl(DWORD dwVirtualKeyAndModifiers)
{
	return MakeHotkey(GetHotkeyVirtualKeyCode(dwVirtualKeyAndModifiers),
		ConvertModifiersFromCtrl(GetHotkeyModifiers(dwVirtualKeyAndModifiers)));
}

DWORD CHotkey::MakeHotkey(WORD wVirtualKey, WORD wModifiers)
{
	return MAKELONG(wVirtualKey, wModifiers);
}

BOOL CHotkey::IsRegisterableHotkey(HWND hWnd, DWORD dwVirtualKeyAndModifiers)
{
	return IsRegisterableHotkey(hWnd, GetHotkeyVirtualKeyCode(dwVirtualKeyAndModifiers), GetHotkeyModifiers(dwVirtualKeyAndModifiers));
}

BOOL CHotkey::IsRegisterableHotkey(HWND hWnd, WORD wVirtualKey, WORD wModifiers)
{
	UINT uHotkeyId;
	if (RegisterHotkey(hWnd, wVirtualKey, wModifiers, uHotkeyId))
	{
		UnRegisterHotkey(hWnd, uHotkeyId);
		return TRUE;
	}
	return FALSE;
}

BOOL CHotkey::RegisterHotkey(HWND hWnd, DWORD dwVirtualKeyAndModifiers, UINT& uIdHotkey)
{
	return RegisterHotkey(hWnd, GetHotkeyModifiers(dwVirtualKeyAndModifiers), GetHotkeyVirtualKeyCode(dwVirtualKeyAndModifiers), uIdHotkey);
}

BOOL CHotkey::RegisterHotkey(HWND hWnd, WORD wVirtualKey, WORD wModifiers, UINT& uIdHotkey)
{
	uIdHotkey = GetFirstFreeHotkeyId();
	return RegisterHotKey(hWnd, uIdHotkey, wModifiers, wVirtualKey);
}

BOOL CHotkey::UnRegisterHotkey(HWND hWnd, UINT uIdHotkey)
{
	return UnregisterHotKey(hWnd, uIdHotkey);
}

//	End of static functions

CHotkey::CHotkey()
{
	m_wVirtualKey = NO_VIRTUAL_KEY;	// 'A';
	m_wModifiers = NO_MODIFIER;	// MOD_ALT|MOD_CONTROL;
	m_hwndOwner = NULL;
	m_uHotkeyID = NO_ID;
}

void CHotkey::operator=(const CHotkey& hotkey)
{
	m_hwndOwner = hotkey.m_hwndOwner;
	m_wVirtualKey = hotkey.m_wVirtualKey;
	m_wModifiers = hotkey.m_wModifiers;
	m_uHotkeyID = hotkey.m_uHotkeyID;
}

BOOL CHotkey::IsValid() const
{
	return m_wVirtualKey != NO_VIRTUAL_KEY && m_wModifiers != NO_MODIFIER;
}

BOOL CHotkey::IsRegistered() const
{
	return m_uHotkeyID != NO_ID;
}

UINT CHotkey::GetHotkeyID() const
{
	return m_uHotkeyID;
}

BOOL CHotkey::RegisterHotkey(HWND hwndOwner, DWORD dwVirtualKeyAndModifiers)
{
	return RegisterHotkey(hwndOwner, GetHotkeyVirtualKeyCode(dwVirtualKeyAndModifiers), GetHotkeyModifiers(dwVirtualKeyAndModifiers));
}

BOOL CHotkey::RegisterHotkey(HWND hwndOwner, WORD wVirtualKey, WORD wModifiers)
{
	ASSERT(!IsRegistered());
	m_hwndOwner = hwndOwner;
	m_wVirtualKey = wVirtualKey;
	m_wModifiers = wModifiers;
	if (m_wVirtualKey == 0 && m_wModifiers == 0) return true;

	BOOL fRet = RegisterHotkey(m_hwndOwner, m_wVirtualKey, m_wModifiers, m_uHotkeyID);
	if (!fRet) m_uHotkeyID = NO_ID;
	LOG3F("RegisterHotkey() %d %08lX: %s\n", m_uHotkeyID, m_hwndOwner, fRet ? "TRUE" : "FALSE");
	return fRet;
}

BOOL CHotkey::UnRegisterHotkey()
{
	if (m_uHotkeyID != NO_ID)
	{
		BOOL fRet = UnRegisterHotkey(m_hwndOwner, m_uHotkeyID);
		LOG3F("UnRegisterHotkey() %d %08lX : %s (%s)\n", m_uHotkeyID, m_hwndOwner, fRet ? "TRUE" : "FALSE", GetLastErrorString());
		m_uHotkeyID = NO_ID;
		return fRet;
	}
	return FALSE;
}

struct tt_hotkey
{
	const char * szLabel;
	WORD wVk;
};

static tt_hotkey s_hotkeys[] = {
	{"F1",VK_F1},{"F2",VK_F2},{"F3",VK_F3},{"F4",VK_F4},{"F5",VK_F5},{"F6",VK_F6},
	{"F7",VK_F7},{"F8",VK_F8},{"F9",VK_F9},{"F10",VK_F10},{"F11",VK_F11},{"F12",VK_F12},
	{"Num0",VK_NUMPAD0},{"Num1",VK_NUMPAD1},{"Num2",VK_NUMPAD2},{"Num3",VK_NUMPAD3},{"Num4",VK_NUMPAD4},
	{"Num5",VK_NUMPAD5},{"Num6",VK_NUMPAD6},{"Num7",VK_NUMPAD7},{"Num8",VK_NUMPAD8},{"Num9",VK_NUMPAD9},
	{"A",'A'},{"B",'B'},{"C",'C'},{"D",'D'},{"E",'E'},{"F",'F'},{"G",'G'},{"H",'H'},{"I",'I'},{"J",'J'},
	{"K",'K'},{"L",'L'},{"M",'M'},{"N",'N'},{"O",'O'},{"P",'P'},{"Q",'Q'},{"R",'R'},{"S",'S'},{"T",'T'},
	{"U",'U'},{"V",'V'},{"W",'W'},{"X",'X'},{"Y",'Y'},{"Z",'Z'},
	{"Page p�c�dente",VK_PRIOR},{"Page suivante",VK_NEXT},{"D�but",VK_HOME},{"Fin",VK_END},
	{"Haut",VK_UP},{"Bas",VK_DOWN},{"Gauche",VK_LEFT},{"Droite",VK_RIGHT},
	{"*",VK_MULTIPLY},{"/",VK_DIVIDE},{"+",VK_ADD},{"-",VK_SUBTRACT},{".",VK_DECIMAL},
	{"Tabulation",VK_TAB},{"Suppr.",VK_DELETE},{"Insert.",VK_INSERT},
	{"Entr�e", VK_RETURN},
	{"Retour arr.",VK_BACK},
	{"Echap",VK_ESCAPE},
	{NULL, NULL},
};

static tt_hotkey s_modifiers[] = {
	/*{"Aucun",0},{"Alt", MOD_ALT},{"Ctrl", MOD_CONTROL},{"Shift", MOD_SHIFT},{"Win",MOD_WIN},
	{"Alt + Win", MOD_ALT+MOD_WIN},
	{"Ctrl + Win",MOD_CONTROL+MOD_WIN},
	{"Shift + Win",MOD_SHIFT+MOD_WIN},
	{"Ctrl + Alt", MOD_ALT|MOD_CONTROL},
	{"Alt + Shift", MOD_ALT|MOD_SHIFT},
	{"Ctrl + Shift", MOD_CONTROL|MOD_SHIFT},
	{"Ctrl + Alt + Shift", MOD_CONTROL|MOD_ALT|MOD_SHIFT},
	{NULL, NULL},
	*/
	{"CTRL", MOD_CONTROL},
	{"MAJ", MOD_SHIFT},
	{"ALT", MOD_ALT},
	{"WIN",MOD_WIN},
};

CString CHotkey::GetHostkeyName(DWORD dwVirtualKeyAndModifiers)
{
	return GetHostkeyName(GetHotkeyVirtualKeyCode(dwVirtualKeyAndModifiers), GetHotkeyModifiers(dwVirtualKeyAndModifiers));
}

CString CHotkey::GetHostkeyName(WORD wVirtualKey, WORD wModifiers)
{
	CString sHotkey;

	LOG2F("Size:%d\n", sizeof(s_modifiers) / sizeof(s_modifiers[0]));
	for (int i = 0; i < sizeof(s_modifiers) / sizeof(s_modifiers[0]); i++)
	{
		if (wModifiers & s_modifiers[i].wVk)
		{
			sHotkey += s_modifiers[i].szLabel;
			sHotkey += " + ";
		}
	}

	for (int i = 0;; i++)
	{
		if (s_hotkeys[i].szLabel == NULL)
		{
			break;
		}
		if (s_hotkeys[i].wVk == wVirtualKey)
		{
			//if (sHotkey != "") sHotkey += " + ";
			sHotkey += s_hotkeys[i].szLabel;
			break;
		}
	}
	if (sHotkey == "")
	{
		sHotkey = "Aucune";
	}
	return sHotkey;
}

CString CHotkey::GetHotkeyName() const
{
	if (!IsValid()) return "[Invalide]";
	return GetHostkeyName(m_wVirtualKey, m_wModifiers);
}
