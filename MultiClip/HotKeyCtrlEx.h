#if !defined(AFX_HOTKEYCTRLEX_H__F157F31F_ED51_4E2C_930D_68978E3E70B7__INCLUDED_)
#define AFX_HOTKEYCTRLEX_H__F157F31F_ED51_4E2C_930D_68978E3E70B7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HotKeyCtrlEx.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CHotKeyCtrlEx window

#include "Hotkey.h"

class CHotKeyCtrlEx : public CHotKeyCtrl
{
	// Construction
public:
	CHotKeyCtrlEx();

	// Attributes
public:

	// Operations
public:

	// Overrides
		// ClassWizard generated virtual function overrides
		//{{AFX_VIRTUAL(CHotKeyCtrlEx)
		//}}AFX_VIRTUAL

	// Implementation
public:
	DWORD GetHotKeyFromCtrl();
	void SetHotKeyToCtrl(DWORD dwVirtualKeyCodeAndModifiers);
	virtual ~CHotKeyCtrlEx();

	// Generated message map functions
protected:
	//{{AFX_MSG(CHotKeyCtrlEx)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HOTKEYCTRLEX_H__F157F31F_ED51_4E2C_930D_68978E3E70B7__INCLUDED_)
